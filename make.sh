#!/bin/bash
cd antlr
mkdir build && mkdir run && cd build
cmake .. -DANTLR_JAR_LOCATION=../antlr-4.7.2-complete.jar -DWITH_DEMO=False
make
(export DESTDIR=../run; make install)
cd ../..
mkdir build
cd build && cmake .. && make
cp Latte ../latc
