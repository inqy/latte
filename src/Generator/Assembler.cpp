#include <set>
#include "Assembler.h"
#include "../Tree/Function.h"
#include "../Value/Int.h"
#include "../Value/Str.h"

std::string G::Assembler::assemble(std::vector<std::shared_ptr<G::Operation>> ops, std::vector<std::string> strings) {
    append(".intel_syntax noprefix");
    assembleStrings(strings);
    append(".globl main");
    for(const auto& op : ops) {
        assembleOperation(op);
    }
    return code.str();
}

void G::Assembler::assembleStrings(const std::vector<std::string>& strings) {
    int i = 0;
    for (const auto& s : strings) {
        this->append(".S" + std::to_string(i) + ":");
        this->indentation++;
        this->append(".asciz \"" + s + "\"");
        this->indentation--;
        i++;
    }
}

void G::Assembler::assembleOperation(std::shared_ptr<G::Operation> op) {
    switch (op->type_()) {
        case G::BIOP: {
            std::shared_ptr<G::BiOperation> bop = std::static_pointer_cast<G::BiOperation>(op);
            std::set<T::BiOperatorType> biTypes = {T::ADD, T::SUB, T::MUL, T::AND, T::OR};
            std::set<T::BiOperatorType> cmpTypes = {T::EQ, T::NE, T::LT, T::LE, T::GT, T::GE};
            if (biTypes.find(bop->operator_) != biTypes.end()) {
                std::string opStr;
                switch (bop->operator_) {
                    case T::ADD:
                        opStr = "add";
                        break;
                    case T::SUB:
                        opStr = "sub";
                        break;
                    case T::MUL:
                        opStr = "imul";
                        break;
                    case T::AND:
                        opStr = "and";
                        break;
                    case T::OR:
                        opStr = "or";
                        break;
                }
                append("mov rax, " + assembleData(bop->left));
                append(opStr + " rax, " + assembleData(bop->right));
                append("mov " + assembleData(bop->target) + ", rax");
            } else if (cmpTypes.find(bop->operator_) != cmpTypes.end()) {
                std::string opStr;
                switch (bop->operator_) {
                    case T::EQ:
                        opStr = "sete";
                        break;
                    case T::NE:
                        opStr = "setne";
                        break;
                    case T::LT:
                        opStr = "setl";
                        break;
                    case T::LE:
                        opStr = "setle";
                        break;
                    case T::GT:
                        opStr = "setg";
                        break;
                    case T::GE:
                        opStr = "setge";
                        break;
                }
                append("mov rax, " + assembleData(bop->left));
                append("cmp rax, " + assembleData(bop->right));
                append("mov rax, 0x0000000000000000");
                append(opStr + " al");
                append("mov " + assembleData(bop->target) + ", rax");
            } else if(bop->operator_ == T::DIV) {
                append("mov rax, " + assembleData(bop->left));
                append("mov rcx, " + assembleData(bop->right));
                append("xor rdx, rdx");
                append("cqo");
                append("idiv rcx");
                append("mov " + assembleData(bop->target) + ", rax");
            } else if(bop->operator_ == T::MOD) {
                append("mov rax, " + assembleData(bop->left));
                append("mov rcx, " + assembleData(bop->right));
                append("xor rdx, rdx");
                append("cqo");
                append("idiv rcx");
                append("mov " + assembleData(bop->target) + ", rdx");
            } else {
                throw std::runtime_error{"no operator"};
            }
            break;
        }
        case G::UNOP: {
            std::shared_ptr<G::UnOperation> bop = std::static_pointer_cast<G::UnOperation>(op);
            switch (bop->operator_) {
                case T::NOT:
                    append("mov rax, " + assembleData(bop->source));
                    append("test rax, rax");
                    append("mov rax, 0x0000000000000000");
                    append("setz al");
                    append("mov " + assembleData(bop->target) + ", rax");
                    break;
                case T::NEG:
                    append("mov rax, " + assembleData(bop->source));
                    append("neg rax");
                    append("mov " + assembleData(bop->target) + ", rax");
                    break;
            }

            break;
        }
        case G::COPY: {
            std::shared_ptr<G::Copy> bop = std::static_pointer_cast<G::Copy>(op);
            append("mov rax, " + assembleData(bop->source));
            append("mov " + assembleData(bop->target) + ", rax");
            break;
        }
        case G::JUMP: {
            std::shared_ptr<G::Jump> bop = std::static_pointer_cast<G::Jump>(op);
            append("jmp .L" + std::to_string(bop->target));
            break;
        }
        case G::LABEL: {
            std::shared_ptr<G::Label> bop = std::static_pointer_cast<G::Label>(op);
            indentation--;
            append(".L" + std::to_string(bop->index) + ":");
            indentation++;
            break;
        }
        case G::CONDJUMP: {
            std::shared_ptr<G::CondJump> bop = std::static_pointer_cast<G::CondJump>(op);
            append("mov rax, " + assembleData(bop->value));
            append("test rax, rax");
            append("jnz .L" + std::to_string(bop->target));
            break;
        }
        case G::PARAM: {
            std::vector<std::string> argRegisters = {"rdi", "rsi", "rdx", "rcx", "r8", "r9"};
            std::shared_ptr<G::Param> bop = std::static_pointer_cast<G::Param>(op);
            if (bop->index < 6) {
                append("mov " + argRegisters[bop->index] + ", " + assembleData(bop->source));
            } else {
                append("mov rax, " + assembleData(bop->source));
                append("push rax");
            }
            break;
        }
        case G::CALL: {
            std::shared_ptr<G::Call> bop = std::static_pointer_cast<G::Call>(op);
            append("call " + bop->function->name);
            append("mov " + assembleData(bop->dest) + ", rax");
            if (bop->function->args.size() > 6) {
                int argsSize = (bop->function->args.size() - 6) * 8;
                append("add rsp, " + std::to_string(argsSize));
            }
            break;
        }
        case G::RET: {
            std::shared_ptr<G::Ret> bop = std::static_pointer_cast<G::Ret>(op);
            if (!bop->nothing) {
                append("mov rax, " + assembleData(bop->from));
            }
            append("mov rsp, rbp");
            append("pop rbp");
            append("ret");
            break;
        }
        case G::FLABEL: {
            std::vector<std::string> argRegisters = {"rdi", "rsi", "rdx", "rcx", "r8", "r9"};
            std::shared_ptr<G::FuncLabel> bop = std::static_pointer_cast<G::FuncLabel>(op);
            indentation--;
            append(bop->function->name + ":");
            indentation++;
            append("push rbp");
            append("mov rbp, rsp");
            int stackSize = bop->function->args.size() + bop->function->variables + 1;
            stackSize += 1 - (stackSize % 2);
            append("sub rsp, " + std::to_string(8 * (stackSize)));
            for (int i = 0; i < bop->function->args.size(); i++) {
                if (i < 6) {
                    append("mov [rbp - " +  std::to_string(8 * (i + 1)) + "], " + argRegisters[i]);
                } else {
                    int offset = (bop->function->args.size() - i + 1) * 8;
                    append("mov rax, [rbp + " + std::to_string(offset) + "]");
                    append("mov [rbp - " +  std::to_string(8 * (i + 1)) + "], rax");
                }
            }
            break;
        }
    }
}

std::string G::Assembler::assembleData(std::shared_ptr<Data> ptr) {
    switch (ptr->type_()) {
        case G::REGISTER: {
            auto reg = std::static_pointer_cast<G::Register>(ptr);
            return "[rbp - " + std::to_string(8 * (reg->num + 1)) + "]";
        }
        case G::CONST: {
            auto cnst = std::static_pointer_cast<G::Const>(ptr);
            switch (cnst->value->type()) {
                case INT: {
                    auto val = std::static_pointer_cast<Integer>(cnst->value);
                    return std::to_string(val->value);
                }
                case STR: {
                    auto val = std::static_pointer_cast<Str>(cnst->value);
                    return val->value;
                }
                case BOOLEAN: {
                    auto val = std::static_pointer_cast<Boolean>(cnst->value);
                    std::string boolStr = val->value ? "1" : "0";
                    return boolStr;
                }
                case OBJECT:
                case VOID:
                    throw std::runtime_error{"wtf"};
            }
            break;
        }
        case G::STRING:
            auto string = std::static_pointer_cast<G::ConstString>(ptr);
            append("lea rcx, [.S" + std::to_string(string->label) + "]");
            return "rcx";
            break;
    }
}

void G::Assembler::append(const std::string& value) {
    code << std::string(4 * indentation, ' ') << value << std::endl;
}
