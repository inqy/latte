#ifndef LATTE_ASSEMBLER_H
#define LATTE_ASSEMBLER_H

#include <string>
#include <memory>
#include <vector>
#include <sstream>
#include "Operation.h"

namespace G {
    class Assembler {
    public:
        std::string assemble(std::vector<std::shared_ptr<G::Operation>>, std::vector<std::string>);
    private:
        void assembleStrings(const std::vector<std::string>& strings);
        void assembleOperation(std::shared_ptr<G::Operation>);
        std::string assembleData(std::shared_ptr<Data>);
        void append(const std::string& value);

        std::stringstream code;
        int indentation = 1;

        // Local state
        int currentFuncArgs;
    };
}


#endif //LATTE_ASSEMBLER_H
