#ifndef LATTE_G_DATA_H
#define LATTE_G_DATA_H

#include <memory>
#include <utility>
#include "../Value/Value.h"

namespace G {
    enum DataType {
        REGISTER,
        CONST,
        STRING,
    };

    class Data {
    public:
        virtual DataType type_() = 0;
    };

    class Register : public Data {
    public:
        explicit Register(int num) : num(num) {}

        DataType type_() override {
            return REGISTER;
        }

        int num;
    };

    class Const : public Data {
    public:
        explicit Const(std::shared_ptr<Value> value) : value(std::move(value)) {}

        DataType type_() override {
            return CONST;
        }

        std::shared_ptr<Value> value;
    };

    class ConstString : public Data {
    public:
        ConstString(int label) : label(label) {}

        DataType type_() override {
            return STRING;
        }

        int label;
    };
}

#endif //LATTE_G_DATA_H
