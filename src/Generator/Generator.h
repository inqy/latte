#ifndef LATTE_G_GENERATOR_H
#define LATTE_G_GENERATOR_H

#include "../Tree/Program.h"
#include "Operation.h"

#include <map>
#include <string>
#include <memory>
#include <set>

namespace G {
    class Generator {
    public:
        std::vector<std::shared_ptr<Operation>> generate(std::shared_ptr<T::Program> prog);
        std::vector<std::string> getStrings();

    private:
        static const int NULL_LABEL = 0;
        void emitStmt(std::shared_ptr<T::Stmt> s);
        void emitWhile(std::shared_ptr<T::While> ptr);
        void emitIf(std::shared_ptr<T::If> ptr);
        void emitIfElse(std::shared_ptr<T::IfElse> ptr);
        void emitVReturn(std::shared_ptr<T::VoidReturn> ptr);
        void emitReturn(std::shared_ptr<T::Return> ptr);
        void emitDecrement(std::shared_ptr<T::Dec> ptr);
        void emitIncrement(std::shared_ptr<T::Inc> ptr);
        void emitAssignment(std::shared_ptr<T::Ass> ptr);
        void emitBlock(std::shared_ptr<T::BlockStmt> ptr);
        void emitSeveral(std::shared_ptr<T::Several> ptr);
        std::shared_ptr<Data> emitFunctionCall(std::shared_ptr<T::StmtFunc> ptr);
        std::shared_ptr<Data> emitExpressionCalculation(std::shared_ptr<T::Expr> ptr);
        int emitLabel();
        std::shared_ptr<Jump> emitJump(int label);
        std::shared_ptr<CondJump> emitConditionalJump(int label, std::shared_ptr<T::Expr> condition);

        std::shared_ptr<Register> firstFreeRegister();
        void freeRegister(std::shared_ptr<Data> data);
        void calculateRegisters(std::shared_ptr<T::Function> f);
        void emplaceArguments(std::shared_ptr<T::Function> f);

        int calculateDepth(T::Stmts v);
        int calculateDepth(std::shared_ptr<T::Stmt> s);
        int calculateDepth(std::shared_ptr<T::Expr> e);

        // Global state:
        std::vector<std::shared_ptr<Operation>> operations;
        std::vector<std::string> strings;
        std::map<std::shared_ptr<T::Expr>, int> exprDepth;
        int labelIndex = 0;

        // Per func state
        int nVariables;
        int nCalculations;
        int nTotalRegisters;

        // Per block state
        std::map<int, bool> registerActive;
        std::map<std::shared_ptr<T::Variable>, std::shared_ptr<Register>> variableAllocation;
    };
}

#endif //LATTE_G_GENERATOR_H
