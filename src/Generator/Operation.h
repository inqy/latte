#ifndef LATTE_G_OPERATION_H
#define LATTE_G_OPERATION_H

#include <utility>

#include "../Tree/Expr.h"
#include "Data.h"

namespace G {
    enum OperationType {
        BIOP,
        UNOP,
        COPY,
        JUMP,
        LABEL,
        FLABEL,
        CONDJUMP,
        PARAM,
        CALL,
        RET
    };

    class Operation {
    public:
        virtual OperationType type_() = 0;
    };

    class BiOperation : public Operation {
    public:
        BiOperation(T::BiOperatorType operator_, std::shared_ptr<Data> target, std::shared_ptr<Data> left, std::shared_ptr<Data> right)
            : operator_(operator_), target(std::move(target)), left(std::move(left)),
              right(std::move(right)) {}

        OperationType type_() override {
            return BIOP;
        }

        T::BiOperatorType operator_;
        std::shared_ptr<Data> target;
        std::shared_ptr<Data> left;
        std::shared_ptr<Data> right;
    };

    class UnOperation : public Operation {
    public:
        UnOperation(T::UnOperatorType operator_, std::shared_ptr<Data> target, std::shared_ptr<Data> source) : operator_(operator_),
                                                                                                               target(std::move(target)),
                                                                                                               source(std::move(source)) {}

        OperationType type_() override {
            return UNOP;
        }

        T::UnOperatorType operator_;
        std::shared_ptr<Data> target;
        std::shared_ptr<Data> source;
    };

    class Copy : public Operation {
    public:
        Copy(std::shared_ptr<Data> target, std::shared_ptr<Data> source) : target(std::move(target)), source(std::move(source)) {}

        OperationType type_() override {
            return COPY;
        }

        std::shared_ptr<Data> target;
        std::shared_ptr<Data> source;
    };

    class Jump : public Operation {
    public:
        explicit Jump(int target) : target(target) {}

        OperationType type_() override {
            return JUMP;
        }

        int target;
    };

    class FuncLabel : public Operation {
    public:
        explicit FuncLabel(std::shared_ptr<T::Function> function) : function(std::move(function)) {}

        OperationType type_() override {
            return FLABEL;
        }

        std::shared_ptr<T::Function> function;
    };

    class Label : public Operation {
    public:
        explicit Label(int index) : index(index) {}

        OperationType type_() override {
            return LABEL;
        }

        int index;
    };

    class CondJump : public Operation {
    public:
        CondJump(std::shared_ptr<Data> value, int target) : value(std::move(value)), target(target) {}

        OperationType type_() override {
            return CONDJUMP;
        }

        std::shared_ptr<Data> value;
        int target;
    };

    class Param : public Operation {
    public:
        explicit Param(std::shared_ptr<Data> source, int index) : source(std::move(source)), index(index) {}

        OperationType type_() override {
            return PARAM;
        }

        std::shared_ptr<Data> source;
        int index;
    };

    class Call : public Operation {
    public:
        Call(std::shared_ptr<T::Function> function, std::shared_ptr<Data> dest) : function(std::move(function)), dest(std::move(dest)) {}

        OperationType type_() override {
            return CALL;
        }

        std::shared_ptr<T::Function> function;
        std::shared_ptr<Data> dest;
    };

    class Ret : public Operation {
    public:
        explicit Ret() : nothing(true), from(0) {}

        explicit Ret(std::shared_ptr<Data> from) : nothing(false), from(from) {}

        OperationType type_() override {
            return RET;
        }

        bool nothing;
        std::shared_ptr<Data> from;
    };
}

#endif //LATTE_G_OPERATION_H
