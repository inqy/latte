#include "Generator.h"
#include "../Value/Int.h"
#include "../Value/Str.h"
#include <iostream>

std::vector<std::shared_ptr<G::Operation>> G::Generator::generate(std::shared_ptr<T::Program> prog) {
    for (const auto& f : prog->functions) {
        std::shared_ptr<Operation> func = std::make_shared<FuncLabel>(f);
        operations.push_back(func);

        calculateRegisters(f);
        emplaceArguments(f);
        emitBlock(f->block);
        emitVReturn(std::make_shared<T::VoidReturn>());
    }
    return operations;
}

void G::Generator::emitBlock(std::shared_ptr<T::BlockStmt> block) {
    auto parentVariableAllocation = variableAllocation;
//    auto parentRegisterActive = registerActive;

    for (const auto& s : block->stmts) {
        emitStmt(s);
    }

    std::map<std::shared_ptr<T::Variable>, std::shared_ptr<Register>> newVariableAllocation;
    for (auto [k, v] : parentVariableAllocation) {
        newVariableAllocation[k] = variableAllocation[k];
    }
    variableAllocation = newVariableAllocation;

    std::map<int, bool> newRegisterActive = std::map<int, bool>();
    for (int i = 0; i < nTotalRegisters; i++) {
        registerActive[i] = false;
    }
    for (auto [k, v] : variableAllocation) {
        newRegisterActive[v->num] = true;
    }

    registerActive = newRegisterActive;
}

void G::Generator::emitStmt(std::shared_ptr<T::Stmt> s) {
    switch(s->type_()) {
        case T::FUN:
            freeRegister(this->emitFunctionCall(std::static_pointer_cast<T::StmtFunc>(s)));
            break;
        case T::WHI:
            this->emitWhile(std::static_pointer_cast<T::While>(s));
            break;
        case T::IF:
            this->emitIf(std::static_pointer_cast<T::If>(s));
            break;
        case T::IFE:
            this->emitIfElse(std::static_pointer_cast<T::IfElse>(s));
            break;
        case T::VRET:
            this->emitVReturn(std::static_pointer_cast<T::VoidReturn>(s));
            break;
        case T::RET:
            this->emitReturn(std::static_pointer_cast<T::Return>(s));
            break;
        case T::DEC:
            this->emitDecrement(std::static_pointer_cast<T::Dec>(s));
            break;
        case T::INC:
            this->emitIncrement(std::static_pointer_cast<T::Inc>(s));
            break;
        case T::ASS:
            this->emitAssignment(std::static_pointer_cast<T::Ass>(s));
            break;
        case T::BLK:
            this->emitBlock(std::static_pointer_cast<T::BlockStmt>(s));
            break;
        case T::SEV:
            this->emitSeveral(std::static_pointer_cast<T::Several>(s));
            break;
    }
}

std::shared_ptr<G::Data> G::Generator::emitFunctionCall(std::shared_ptr<T::StmtFunc> ptr) {
    std::vector<std::shared_ptr<Data>> results;
    for (const auto& arg : ptr->args) {
        results.push_back(emitExpressionCalculation(arg));
    }

    int i = 0;
    for (const auto& result : results) {
        std::shared_ptr<Operation> paramOp = std::make_shared<Param>(result, i++);
        operations.push_back(paramOp);
    }

    auto dest = firstFreeRegister();
    std::shared_ptr<Operation> call = std::make_shared<Call>(ptr->function, dest);
    operations.push_back(call);

    for (const auto& result : results) {
        freeRegister(result);
    }
    return dest;
}

void G::Generator::emitWhile(std::shared_ptr<T::While> ptr) {
    std::shared_ptr<Jump> jumpToEnd = emitJump(NULL_LABEL);
    int l0 = emitLabel();
    emitStmt(ptr->body);
    int l1 = emitLabel();
    emitConditionalJump(l0, ptr->condition);
    jumpToEnd->target = l1;
}

void G::Generator::emitIf(std::shared_ptr<T::If> ptr) {
    if (ptr->condition->exprType() == T::BOp) {
        std::shared_ptr<T::BiOperator> biOp = std::static_pointer_cast<T::BiOperator>(ptr->condition);
        if (biOp->type == T::AND || biOp->type == T::OR) {
            std::shared_ptr<CondJump> jump = emitConditionalJump(NULL_LABEL, ptr->condition);
            std::shared_ptr<Jump> elseJump = emitJump(NULL_LABEL);
            int l0 = emitLabel();
            jump->target = l0;
            emitStmt(ptr->body);
            int l1 = emitLabel();
            elseJump->target = l1;

            return;
        }
    }

    std::shared_ptr<T::Expr> notCondition = std::make_shared<T::UnOperator>(T::NOT, ptr->condition);
    std::shared_ptr<CondJump> jump = emitConditionalJump(NULL_LABEL, notCondition);
    emitStmt(ptr->body);
    int l0 = emitLabel();
    jump->target = l0;
}

void G::Generator::emitIfElse(std::shared_ptr<T::IfElse> ptr) {
    std::shared_ptr<CondJump> jump = emitConditionalJump(NULL_LABEL, ptr->condition);
    emitStmt(ptr->elseBody);
    std::shared_ptr<Jump> exitJump = emitJump(NULL_LABEL);
    int l0 = emitLabel();
    emitStmt(ptr->thenBody);
    int l1 = emitLabel();
    jump->target = l0;
    exitJump->target = l1;
}

void G::Generator::emitVReturn(std::shared_ptr<T::VoidReturn> ptr) {
    std::shared_ptr<Operation> ret = std::make_shared<Ret>();
    operations.push_back(ret);
}

void G::Generator::emitReturn(std::shared_ptr<T::Return> ptr) {
    auto data = emitExpressionCalculation(ptr->expr);
    std::shared_ptr<Operation> ret = std::make_shared<Ret>(data);
    operations.push_back(ret);
}

void G::Generator::emitDecrement(std::shared_ptr<T::Dec> ptr) {
    std::shared_ptr<Value> one = std::make_shared<Integer>(1);
    std::shared_ptr<Data> const_ = std::make_shared<Const>(one);
    std::shared_ptr<Data> target = variableAllocation[ptr->var];
    std::shared_ptr<Operation> dec = std::make_shared<BiOperation>(T::SUB, target, target, const_);
    operations.push_back(dec);
}

void G::Generator::emitIncrement(std::shared_ptr<T::Inc> ptr) {
    std::shared_ptr<Value> one = std::make_shared<Integer>(1);
    std::shared_ptr<Data> const_ = std::make_shared<Const>(one);
    std::shared_ptr<Data> target = variableAllocation[ptr->var];
    std::shared_ptr<Operation> dec = std::make_shared<BiOperation>(T::ADD, target, target, const_);
    operations.push_back(dec);
}

void G::Generator::emitAssignment(std::shared_ptr<T::Ass> ptr) {
    auto data = emitExpressionCalculation(ptr->value);
    if (variableAllocation.find(ptr->var) == variableAllocation.end()) {
        bool found = false;
        for (int i = 0; i < nVariables; i++) {
            if (!registerActive[i]) {
                found = true;
                registerActive[i] = true;
                variableAllocation[ptr->var] = std::make_shared<Register>(i);
                break;
            }
        }
        if (!found) {
            throw std::runtime_error{"run out of registers lol"};
        }
    }
    std::shared_ptr<Operation> op = std::make_shared<Copy>(variableAllocation[ptr->var], data);
    operations.push_back(op);
    freeRegister(data);
}

void G::Generator::emitSeveral(std::shared_ptr<T::Several> ptr) {
    for (auto s : ptr->stmts) {
        emitStmt(s);
    }
}

std::shared_ptr<G::Data> G::Generator::emitExpressionCalculation(std::shared_ptr<T::Expr> ptr) {
    switch (ptr->exprType()) {
        case T::UOp: {
            std::shared_ptr<T::UnOperator> unOp = std::static_pointer_cast<T::UnOperator>(ptr);
            auto src = emitExpressionCalculation(unOp->expr);
            auto target = firstFreeRegister();
            std::shared_ptr<Operation> op = std::make_shared<UnOperation>(unOp->type, target, src);
            this->operations.push_back(op);
            freeRegister(src);
            return target;
        }
        case T::BOp: {
            std::shared_ptr<T::BiOperator> biOp = std::static_pointer_cast<T::BiOperator>(ptr);
            if (biOp->type == T::AND) {
                std::shared_ptr<T::Expr> notLeft = std::make_shared<T::UnOperator>(T::NOT, biOp->left);
                std::shared_ptr<T::Expr> notRight = std::make_shared<T::UnOperator>(T::NOT, biOp->right);
                std::shared_ptr<CondJump> leftJump = emitConditionalJump(NULL_LABEL, notLeft);
                std::shared_ptr<CondJump> rightJump = emitConditionalJump(NULL_LABEL, notRight);
                auto target = firstFreeRegister();
                // 1
                std::shared_ptr<Value> true_ = std::make_shared<Boolean>(true);
                std::shared_ptr<Operation> op1 = std::make_shared<Copy>(target, std::make_shared<G::Const>(true_));
                this->operations.push_back(op1);
                auto exitJump = emitJump(NULL_LABEL);
                // 0
                int l0 = emitLabel();
                leftJump->target = l0;
                rightJump->target = l0;
                std::shared_ptr<Value> false_ = std::make_shared<Boolean>(false);
                std::shared_ptr<Operation> op0 = std::make_shared<Copy>(target, std::make_shared<G::Const>(false_));
                this->operations.push_back(op0);
                // exit
                int l1 = emitLabel();
                exitJump->target = l1;

                return target;
            } else if (biOp->type == T::OR) {
                std::shared_ptr<CondJump> leftJump = emitConditionalJump(NULL_LABEL, biOp->left);
                std::shared_ptr<CondJump> rightJump = emitConditionalJump(NULL_LABEL, biOp->right);
                auto target = firstFreeRegister();
                // 0
                std::shared_ptr<Value> false_ = std::make_shared<Boolean>(false);
                std::shared_ptr<Operation> op0 = std::make_shared<Copy>(target, std::make_shared<G::Const>(false_));
                this->operations.push_back(op0);
                auto exitJump = emitJump(NULL_LABEL);
                // 1
                int l0 = emitLabel();
                leftJump->target = l0;
                rightJump->target = l0;
                std::shared_ptr<Value> true_ = std::make_shared<Boolean>(true);
                std::shared_ptr<Operation> op1 = std::make_shared<Copy>(target, std::make_shared<G::Const>(true_));
                this->operations.push_back(op1);
                // exit
                int l1 = emitLabel();
                exitJump->target = l1;

                return target;
            } else {
                auto left = emitExpressionCalculation(biOp->left);
                auto right = emitExpressionCalculation(biOp->right);
                freeRegister(left);
                freeRegister(right);
                auto target = firstFreeRegister();
                std::shared_ptr<Operation> op = std::make_shared<BiOperation>(biOp->type, target, left, right);
                this->operations.push_back(op);
                return target;
            }
        }
        case T::Con: {
            std::shared_ptr<T::Const> constT = std::static_pointer_cast<T::Const>(ptr);
            if (constT->value->type() == STR) {
                std::shared_ptr<Str> string = Str::toStr(constT->value);
                strings.push_back(string->value);
                return std::make_shared<ConstString>(strings.size() - 1);
            } else {
                return std::make_shared<Const>(constT->value);
            }
        }
        case T::FCall:
            return emitFunctionCall(T::StmtFunc::fromFuncCall(std::static_pointer_cast<T::FuncCall>(ptr)));
        case T::VAcc:
            std::shared_ptr<T::VariableAccess> access = std::static_pointer_cast<T::VariableAccess>(ptr);
            if (variableAllocation.find(access->variable) == variableAllocation.end()) {
                throw std::runtime_error{"Variable unallocated?"};
            }
            return variableAllocation[access->variable];
    }
}

int G::Generator::emitLabel() {
    int index = labelIndex++;
    std::shared_ptr<Operation> label = std::make_shared<Label>(index);
    operations.push_back(label);
    return index;
}

std::shared_ptr<G::Jump> G::Generator::emitJump(int label) {
    std::shared_ptr<Operation> op = std::make_shared<Jump>(label);
    operations.push_back(op);
    return std::static_pointer_cast<Jump>(op);
}

std::shared_ptr<G::CondJump> G::Generator::emitConditionalJump(int label, std::shared_ptr<T::Expr> condition) {
    auto value = emitExpressionCalculation(condition);
    std::shared_ptr<Operation> op = std::make_shared<CondJump>(value, label);
    operations.push_back(op);
    freeRegister(value);
    return std::static_pointer_cast<CondJump>(op);
}

int G::Generator::calculateDepth(T::Stmts stmts) {
    int maxStmtDepth = 0;
    for (auto stmt : stmts) {
        maxStmtDepth = std::max(maxStmtDepth, calculateDepth(stmt));
    }
    return maxStmtDepth;
}

int G::Generator::calculateDepth(std::shared_ptr<T::Stmt> s) {
    switch(s->type_()) {
        case T::FUN: {
            auto ptr = std::static_pointer_cast<T::StmtFunc>(s);
            int maxArgDepth = 0;
            for (const auto& arg : ptr->args) {
                maxArgDepth = std::max(maxArgDepth, calculateDepth(arg));
            }
            return maxArgDepth + ptr->args.size() + 1;
        }
        case T::WHI: {
            auto ptr = std::static_pointer_cast<T::While>(s);
            return std::max(calculateDepth(ptr->body), calculateDepth(ptr->condition));
        }
        case T::IF: {
            auto ptr = std::static_pointer_cast<T::If>(s);
            return std::max(calculateDepth(ptr->body), calculateDepth(ptr->condition));
        }
        case T::IFE: {
            auto ptr = std::static_pointer_cast<T::IfElse>(s);
            return std::max(calculateDepth(ptr->condition), std::max(calculateDepth(ptr->thenBody), calculateDepth(ptr->elseBody)));
        }
        case T::VRET: {
            auto ptr = std::static_pointer_cast<T::VoidReturn>(s);
            return 0;
        }
        case T::RET: {
            auto ptr = std::static_pointer_cast<T::Return>(s);
            return calculateDepth(ptr->expr) + 1;
        }
        case T::DEC: {
            auto ptr = std::static_pointer_cast<T::Dec>(s);
            return 1;
        }
        case T::INC: {
            auto ptr = std::static_pointer_cast<T::Inc>(s);
            return 1;
        }
        case T::ASS: {
            auto ptr = std::static_pointer_cast<T::Ass>(s);
            return calculateDepth(ptr->value);
        }
        case T::BLK: {
            auto ptr = std::static_pointer_cast<T::BlockStmt>(s);
            return calculateDepth(ptr->stmts);
        }
        case T::SEV: {
            auto ptr = std::static_pointer_cast<T::Several>(s);
            return calculateDepth(ptr->stmts);
        }
    }
}

int G::Generator::calculateDepth(std::shared_ptr<T::Expr> ptr) {
    switch (ptr->exprType()) {
        case T::UOp: {
            //std::shared_ptr<T::UnOperator> unOp = std::static_pointer_cast<T::UnOperator>(ptr);
            return 1;
        }
        case T::BOp: {
            std::shared_ptr<T::BiOperator> biOp = std::static_pointer_cast<T::BiOperator>(ptr);
            int left = calculateDepth(biOp->left);
            int right = calculateDepth(biOp->right);
            int depth = std::max(std::max(left, right), std::min(left, right) + 1);
            exprDepth[ptr] = depth;
            return depth;
        }
        case T::Con: {
            //std::shared_ptr<T::Const> constT = std::static_pointer_cast<T::Const>(ptr);
            return 0;
        }
        case T::FCall: {
            auto memtr = std::static_pointer_cast<T::FuncCall>(ptr);
            int maxArgDepth = 0;
            for (const auto& arg : memtr->args) {
                maxArgDepth = std::max(maxArgDepth, calculateDepth(arg));
            }
            return maxArgDepth + memtr->args.size() + 1;
        }
        case T::VAcc:
            //std::shared_ptr<T::VariableAccess> access = std::static_pointer_cast<T::VariableAccess>(ptr);
            return 0;
    }
}

void G::Generator::calculateRegisters(std::shared_ptr<T::Function> f) {
    nVariables = f->variables + f->args.size();
    nCalculations = calculateDepth(f->block->stmts);
    nTotalRegisters = nVariables + nCalculations;
    registerActive = std::map<int, bool>();
    for (int i = 0; i < nTotalRegisters; i++) {
        registerActive[i] = false;
    }
    variableAllocation = std::map<std::shared_ptr<T::Variable>, std::shared_ptr<Register>>();
//    std::cout << "nVariables:" << nVariables << "\n";
//    std::cout << "nCalculations:" << nCalculations << "\n";
//    std::cout << "nTotalRegisters:" << nTotalRegisters << "\n";
}

std::shared_ptr<G::Register> G::Generator::firstFreeRegister() {
//    std::cout << "Looking at registers:\n";
    for (int i = nVariables; i < nTotalRegisters; i++) {
//        std::cout << i << " ";
        if (!registerActive[i]) {
            registerActive[i] = true;
//            std::cout << "\n";
            return std::make_shared<Register>(i);
        }
    }

//    std::cout << "/**********************************************************/\n";
//    PrettyPrinter::printCode(operations);
    throw std::runtime_error{"run out of registers"};
}

void G::Generator::freeRegister(std::shared_ptr<Data> data) {
    if (data->type_() == REGISTER) {
        std::shared_ptr<Register> reg = std::dynamic_pointer_cast<Register>(data);
        for (const auto& [k, v] : variableAllocation) {
            if (v == reg) {
                return;
            }
        }
        registerActive[reg->num] = false;
    }
}

void G::Generator::emplaceArguments(std::shared_ptr<T::Function> f) {
    int i = 0;
    for (auto arg : f->args) {
        bool found = false;
        for (; i < nVariables; i++) {
            if (!registerActive[i]) {
                found = true;
                registerActive[i] = true;
                variableAllocation[arg->var] = std::make_shared<Register>(i);
                break;
            }
        }
        if (!found) {
            throw std::runtime_error{"run out of registers"};
        }
    }
}

std::vector<std::string> G::Generator::getStrings() {
    return strings;
}
