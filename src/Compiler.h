#ifndef LATTE_COMPILER_H
#define LATTE_COMPILER_H

#include <string>

class Compiler {
public:
    int compile(std::string filepath);
};

#endif //LATTE_COMPILER_H
