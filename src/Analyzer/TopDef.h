#ifndef LATTE_TOPDEF_H
#define LATTE_TOPDEF_H

#include <string>
#include <utility>

namespace A {
    class TopDef {
    public:
        explicit TopDef(std::string name) : name(std::move(name)) {}

        const std::string name;
    };
}

#endif
