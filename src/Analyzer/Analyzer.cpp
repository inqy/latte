#include "Analyzer.h"
#include "TopDef.h"
#include "Error.h"
#include "Result.h"
#include "Arg.h"
#include "Variable.h"
#include "Block.h"
#include "Item.h"
#include "Expr.h"
#include "../Value/Value.h"
#include "Stmt.h"
#include "../Operators.h"
#include "../Value/Int.h"
#include "../Value/Str.h"
#include "../Value/Boolean.h"
#include "../Value/Null.h"
#include "Function.h"
#include "LValue.h"

antlrcpp::Any A::Analyzer::visitProgram(LatteParser::ProgramContext* context) {
    emplacePredefinedFunctions();

    if (gatherTopDefsMode) {
        std::vector<TopDef> topDefResults{};
        for (auto* topDef : context->topDef()) {
            topDefResults.push_back(visit(topDef).as<TopDef>());
        }
        bool isMainMissing = std::none_of(topDefResults.begin(), topDefResults.end(),
                                          [](const TopDef& result) { return result.name == "main"; });
        if (isMainMissing) {
            reportError(MISSING_MAIN, context->getStart());
        }
        gatherTopDefsMode = false;
    }

    std::vector<TopDef> topDefResults{};
    for (auto* topDef : context->topDef()) {
        topDefResults.push_back(visit(topDef).as<TopDef>());
    }

    Result result{errors};
    return antlrcpp::Any(std::move(result));
}

antlrcpp::Any A::Analyzer::visitTopDef(LatteParser::TopDefContext* context) {
    const std::string& functionName = context->ID()->getText();
    Type type = visit(context->type_()).as<Type>();

    if (gatherTopDefsMode) {
        if (functions.find(functionName) != functions.end()) {
            reportError(DUPLICATE_FUNCTION_NAME, context->getStart());
        }
        std::vector<Argument> args{};
        if (context->arg() != nullptr) {
            Arg argResult = visit(context->arg()).as<Arg>();
            args = argResult.arguments;
        }
        std::shared_ptr<Function> f = std::make_shared<Function>(type, args, functionName);
        functions.emplace(std::make_pair(functionName, f));
        return antlrcpp::Any(TopDef{functionName});
    }

    Variables parentVariables = variables;
    for (const auto& arg : functions.at(functionName)->args) {
        std::shared_ptr<Value> value = unknown(arg.type);
        std::shared_ptr<Variable> variable = std::make_shared<Variable>(arg.name, arg.type, value, depth + 1);
        variables.emplace(arg.name, variable);
    }

    currentFuncReturnType = type;

    Block block = visit(context->block()).as<Block>();
    if (!block.returned && type != VOID) {
        reportError(FUNCTION_DOES_NOT_RETURN, context->getStart());
    }
    if (block.returnType != type) {
        reportError(FUNCTION_RETURNS_WRONG_TYPE, context->getStart());
    }
    variables = parentVariables;

    TopDef result = TopDef{functionName};
    return antlrcpp::Any(std::move(result));
}

antlrcpp::Any A::Analyzer::visitArg(LatteParser::ArgContext* context) {
    std::set<std::string> argumentNames;
    std::vector<Argument> arguments;
    for (int i = 0; i < context->ID().size(); i++) {
        auto* id = context->ID(i);
        Type type = visit(context->type_(i));
        std::string name = id->getText();

        if (argumentNames.find(name) != argumentNames.end()) {
            reportError(DUPLICATE_ARGUMENT_NAME, id->getSymbol());
        }

        argumentNames.insert(name);
        arguments.emplace_back(name, type);
    }

    Arg result{std::move(arguments)};
    return antlrcpp::Any(result);
}

antlrcpp::Any A::Analyzer::visitBlock(LatteParser::BlockContext* ctx) {
    Variables parentVariables = variables;
    depth++;
    Stmt result;
    for (auto* stmt : ctx->stmt()) {
        result = visit(stmt).as<Stmt>();
    }

    depth--;
    variables = parentVariables;
    Block blockResult{result.returned, result.returnType};
    return antlrcpp::Any(blockResult);
}

antlrcpp::Any A::Analyzer::visitEmpty(LatteParser::EmptyContext* ctx) {
    Stmt result{};
    return antlrcpp::Any(result);
}

antlrcpp::Any A::Analyzer::visitBlockStmt(LatteParser::BlockStmtContext* ctx) {
    Block result = visit(ctx->block());
    return result.returned ? Stmt{result.returnType} : Stmt{};
}

antlrcpp::Any A::Analyzer::visitDecl(LatteParser::DeclContext* ctx) {
    Type type = visit(ctx->type_()).as<Type>();
    for (auto* item : ctx->item()) {
        Item itemResult = visit(item).as<Item>();

        if (variables.find(itemResult.name) != variables.end()) {
            std::shared_ptr<Variable> existingVariable = variables.at(itemResult.name);
            if (existingVariable->depth == depth) {
                reportError(REDECLARATION_OF_VARIABLE, item->getStart());
            }
        }
        if (itemResult.value->type() != VOID && itemResult.value->type() != type) {
            reportInvalidExpressionTypeError(type, itemResult.value->type(), item->getStart());
        }
        std::shared_ptr<Value> value = itemResult.value->type() != VOID ? itemResult.value : nullOf(type);
        std::shared_ptr<Variable> variable = std::make_shared<Variable>(itemResult.name, type, value, depth);
        variables[itemResult.name] = variable;
    }

    Stmt result{};
    return antlrcpp::Any(result);
}

antlrcpp::Any A::Analyzer::visitAss(LatteParser::AssContext* ctx) {
    std::shared_ptr<LValue> value = visit(ctx->lvalue()).as<std::shared_ptr<LValue>>();
    if (value->tIdent() == LVALUE_FUNC) {
        reportError(ATTEMPT_TO_USE_FUNCTION_AS_VARIABLE, ctx->getStart());
    }

    Expr exprResult = visit(ctx->expr()).as<Expr>();
    std::shared_ptr<Variable> variable = std::static_pointer_cast<Variable>(value);

    if (exprResult.value->type() != variable->type) {
        reportInvalidExpressionTypeError(variable->type, exprResult.value->type(), ctx->getStart());
    }

    variable->value = exprResult.value;

    return Stmt{};
}

antlrcpp::Any A::Analyzer::visitIncr(LatteParser::IncrContext* ctx) {
    std::shared_ptr<LValue> lvalue = visit(ctx->lvalue()).as<std::shared_ptr<LValue>>();
    if (lvalue->tIdent() == LVALUE_FUNC) {
        reportError(ATTEMPT_TO_USE_FUNCTION_AS_VARIABLE, ctx->getStart());
    }

    std::shared_ptr<Variable> variable = std::static_pointer_cast<Variable>(lvalue);
    if (variable->type != INT) {
        reportInvalidExpressionTypeError(INT, variable->type, ctx->getStart());
    }

    int intResult = std::static_pointer_cast<Integer>(variable->value)->value + 1;
    std::shared_ptr<Value> newValue = std::make_shared<Integer>(intResult);
    variable->value = newValue;

    return Stmt{};
}

antlrcpp::Any A::Analyzer::visitDecr(LatteParser::DecrContext* ctx) {
    std::shared_ptr<LValue> lvalue = visit(ctx->lvalue()).as<std::shared_ptr<LValue>>();
    if (lvalue->tIdent() == LVALUE_FUNC) {
        reportError(ATTEMPT_TO_USE_FUNCTION_AS_VARIABLE, ctx->getStart());
    }

    std::shared_ptr<Variable> variable = std::static_pointer_cast<Variable>(lvalue);
    if (variable->type != INT) {
        reportInvalidExpressionTypeError(INT, variable->type, ctx->getStart());
    }

    int intResult = std::static_pointer_cast<Integer>(variable->value)->value - 1;
    std::shared_ptr<Value> newValue = std::make_shared<Integer>(intResult);
    variable->value = newValue;

    return Stmt{};
}

antlrcpp::Any A::Analyzer::visitRet(LatteParser::RetContext* ctx) {
    Expr exprResult = visit(ctx->expr()).as<Expr>();
    if (currentFuncReturnType != exprResult.value->type()) {
        reportError(FUNCTION_RETURNS_WRONG_TYPE, ctx->getStart());
    }
    return antlrcpp::Any(Stmt{exprResult.value->type()});
}

antlrcpp::Any A::Analyzer::visitVRet(LatteParser::VRetContext* ctx) {
    if (currentFuncReturnType != VOID) {
        reportError(FUNCTION_RETURNS_WRONG_TYPE, ctx->getStart());
    }
    return antlrcpp::Any(Stmt{VOID});
}

antlrcpp::Any A::Analyzer::visitCond(LatteParser::CondContext* ctx) {
    Expr condition = visit(ctx->expr()).as<Expr>();
    if (condition.value->type() != BOOLEAN) {
        reportInvalidExpressionTypeError(BOOLEAN, condition.value->type(), ctx->expr()->getStart());
    }
    std::shared_ptr<Boolean> conditionValue = std::static_pointer_cast<Boolean>(condition.value);

    Stmt ifResult = visit(ctx->stmt()).as<Stmt>();

    Stmt result = (conditionValue->isUnknown() || conditionValue->value)
        ? ifResult
        : Stmt{};
    return antlrcpp::Any(result);
}

antlrcpp::Any A::Analyzer::visitCondElse(LatteParser::CondElseContext* ctx) {
    Expr condition = visit(ctx->expr()).as<Expr>();
    if (condition.value->type() != BOOLEAN) {
        reportInvalidExpressionTypeError(BOOLEAN, condition.value->type(), ctx->expr()->getStart());
    }

    std::shared_ptr<Boolean> boolValue = std::static_pointer_cast<Boolean>(condition.value);

    Stmt ifResult = visit(ctx->stmt(0)).as<Stmt>();
    Stmt elseResult = visit(ctx->stmt(1)).as<Stmt>();

    if (boolValue->isUnknown()) {
        if (ifResult.returned && elseResult.returned) {
            return antlrcpp::Any(ifResult);
        } else {
            return antlrcpp::Any(Stmt{ifResult.returnType});
        }
    } else if (boolValue->value) {
        return antlrcpp::Any(ifResult);
    } else {
        return antlrcpp::Any(elseResult);
    }
}

antlrcpp::Any A::Analyzer::visitWhile(LatteParser::WhileContext* ctx) {
    Expr condition = visit(ctx->expr()).as<Expr>();
    if (condition.value->type() != BOOLEAN) {
        reportInvalidExpressionTypeError(BOOLEAN, condition.value->type(), ctx->expr()->getStart());
    }
    visit(ctx->stmt()); // (!!) TODO: invalidate anything done inside if cond not constexpression
    return antlrcpp::Any(Stmt{});
}

antlrcpp::Any A::Analyzer::visitSExp(LatteParser::SExpContext* ctx) {
    visit(ctx->expr());
    return antlrcpp::Any(Stmt{});
}

antlrcpp::Any A::Analyzer::visitInt(LatteParser::IntContext* ctx) {
    return antlrcpp::Any(INT);
}

antlrcpp::Any A::Analyzer::visitStr(LatteParser::StrContext* ctx) {
    return antlrcpp::Any(STR);
}

antlrcpp::Any A::Analyzer::visitBool(LatteParser::BoolContext* ctx) {
    return antlrcpp::Any(BOOLEAN);
}

antlrcpp::Any A::Analyzer::visitVoid(LatteParser::VoidContext* ctx) {
    return antlrcpp::Any(VOID);
}

antlrcpp::Any A::Analyzer::visitItem(LatteParser::ItemContext* ctx) {
    std::string name = ctx->ID()->getText();
    if (ctx->expr() != nullptr) {
        Expr exprResult = visit(ctx->expr()).as<Expr>();
        return antlrcpp::Any(Item{name, exprResult.value});
    }
    std::shared_ptr<Value> nul = std::make_shared<Null>();
    return antlrcpp::Any(Item{name, nul});
}

antlrcpp::Any A::Analyzer::visitELvalue(LatteParser::ELvalueContext* ctx) {
    std::shared_ptr<LValue> value = visit(ctx->lvalue()).as<std::shared_ptr<LValue>>();
    if (value->tIdent() == LVALUE_FUNC) {
        reportError(ATTEMPT_TO_USE_FUNCTION_AS_VARIABLE, ctx->getStart());
    }
    std::shared_ptr<Variable> var = std::static_pointer_cast<Variable>(value);
    Expr exprResult{var->value};
    return antlrcpp::Any(exprResult);
}

antlrcpp::Any A::Analyzer::visitEFunCall(LatteParser::EFunCallContext* ctx) {
    std::shared_ptr<LValue> value = visit(ctx->lvalue()).as<std::shared_ptr<LValue>>();
    if (value->tIdent() == LVALUE_VAR) {
        reportError(ATTEMPT_TO_USE_VARIABLE_AS_FUNCTION, ctx->getStart());
    }
    auto func = std::static_pointer_cast<Function>(value);
    if (func->args.size() != ctx->expr().size()) {
        reportError(INVALID_NUMBER_OF_ARGUMENTS, ctx->getStart());
    }

    for(int i = 0; i < ctx->expr().size(); i++) {
        auto* expr = ctx->expr().at(i);
        Expr exprResult = visit(expr).as<Expr>();
        if (exprResult.value->type() != func->args.at(i).type) {
            reportError(INVALID_ARGUMENT_TYPE, ctx->getStart());
        }
    }

    std::shared_ptr<Value> result =  func->type != VOID
        ? unknown(func->type)
        : Null::null();

    return Expr{result};
}

antlrcpp::Any A::Analyzer::visitERelOp(LatteParser::ERelOpContext* ctx) {
    Expr left = visit(ctx->expr().at(0)).as<Expr>();
    Expr right = visit(ctx->expr().at(1)).as<Expr>();

    RelationOperator operator_ = visit(ctx->relOp());

    std::shared_ptr<Value> result;
    try {
        switch(operator_) {
            case EQ:
                result = *(left.value) == *(right.value);
                break;
            case NE:
                result = *(left.value) != *(right.value);
                break;
            case GE:
                result = *(left.value) >= *(right.value);
                break;
            case GT:
                result = *(left.value) > *(right.value);
                break;
            case LE:
                result = *(left.value) <= *(right.value);
                break;
            case LT:
                result = *(left.value) < *(right.value);
                break;
            default:
                throw UnsupportedOperationException{"Analyzer::visitERelOp unknown operator"};
        }
    } catch (InvalidTypeException &e) {
        reportInvalidExpressionTypeError(left.value->type(), right.value->type(), ctx->getStart());
    }

    return antlrcpp::Any(Expr{result});
}

antlrcpp::Any A::Analyzer::visitETrue(LatteParser::ETrueContext* ctx) {
    std::shared_ptr<Value> value = std::make_shared<Boolean>(true);
    return antlrcpp::Any(Expr{value});
}

antlrcpp::Any A::Analyzer::visitEOr(LatteParser::EOrContext* ctx) {
    Expr left = visit(ctx->expr().at(0)).as<Expr>();
    Expr right = visit(ctx->expr().at(1)).as<Expr>();

    std::shared_ptr<Value> value;
    try {
        value = *(left.value) || *(right.value);
    } catch (InvalidTypeException& e) {
        reportInvalidExpressionTypeError(BOOLEAN, right.value->type(), ctx->getStart());
    }

    return antlrcpp::Any(Expr{value});
}

antlrcpp::Any A::Analyzer::visitEInt(LatteParser::EIntContext* ctx) {
    try {
        int value = std::stoi(ctx->getText());
        return antlrcpp::Any(Expr{std::make_shared<Integer>(value)});
    } catch (const std::out_of_range& e) {
        reportError(INTEGER_OUT_OF_RANGE, ctx->getStart());
    }
}

antlrcpp::Any A::Analyzer::visitEUnOp(LatteParser::EUnOpContext* ctx) {
    Expr exprResult = visit(ctx->expr()).as<Expr>();
    try {
        if (ctx->getText()[0] == '-') {
            return antlrcpp::Any(Expr{-(*(exprResult.value))});
        } else if (ctx->getText()[0] == '!') {
            return antlrcpp::Any(Expr{!(*(exprResult.value))});
        } else {
            throw UnsupportedOperationException("Analyzer::visitEUnUp unknown operator");
        }
    } catch (InvalidTypeException& e) {
        // TODO:
        reportInvalidExpressionTypeError(VOID, VOID, ctx->getStart());
    }
}

antlrcpp::Any A::Analyzer::visitEStr(LatteParser::EStrContext* ctx) {
    return antlrcpp::Any(Expr{std::make_shared<Str>(ctx->getText())});
}

antlrcpp::Any A::Analyzer::visitEMulOp(LatteParser::EMulOpContext* ctx) {
    Expr left = visit(ctx->expr().at(0)).as<Expr>();
    Expr right = visit(ctx->expr().at(1)).as<Expr>();

    MulOperator operator_ = visit(ctx->mulOp());

    std::shared_ptr<Value> result;
    try {
        switch (operator_) {
            case MUL:
                result = *(left.value) * *(right.value);
                break;
            case DIV:
                result = *(left.value) / *(right.value);
                break;
            case MOD:
                result = *(left.value) % *(right.value);
                break;
            default:
                throw UnsupportedOperationException{"Analyzer::EMulOp unknown operator"};
        }
    } catch (InvalidTypeException &e) {
        reportInvalidExpressionTypeError(left.value->type(), right.value->type(), ctx->getStart());
    }

    return antlrcpp::Any(Expr{result});
}

antlrcpp::Any A::Analyzer::visitEAnd(LatteParser::EAndContext* ctx) {
    Expr left = visit(ctx->expr().at(0)).as<Expr>();
    Expr right = visit(ctx->expr().at(1)).as<Expr>();

    std::shared_ptr<Value> value;
    try {
        value = *(left.value) && *(right.value);
    } catch (InvalidTypeException& e) {
        reportInvalidExpressionTypeError(BOOLEAN, right.value->type(), ctx->getStart());
    }

    return antlrcpp::Any(Expr{value});
}

antlrcpp::Any A::Analyzer::visitEParen(LatteParser::EParenContext* ctx) {
    return visit(ctx->expr());
}

antlrcpp::Any A::Analyzer::visitEFalse(LatteParser::EFalseContext* ctx) {
    std::shared_ptr<Value> value = std::make_shared<Boolean>(false);
    return antlrcpp::Any(Expr{value});
}

antlrcpp::Any A::Analyzer::visitEAddOp(LatteParser::EAddOpContext* ctx) {
    Expr left = visit(ctx->expr().at(0)).as<Expr>();
    Expr right = visit(ctx->expr().at(1)).as<Expr>();

    AddOperator operator_ = visit(ctx->addOp());

    std::shared_ptr<Value> result;
    try {
        switch(operator_) {
            case ADD:
                result = *(left.value) + *(right.value);
                break;
            case SUB:
                result = *(left.value) - *(right.value);
                break;
            default:
                throw UnsupportedOperationException{"Analyzer::visitEAddOp unknown operator"};
        }
    } catch (InvalidTypeException& e) {
        reportInvalidExpressionTypeError(left.value->type(), right.value->type(), ctx->getStart());
    }

    return antlrcpp::Any(Expr{result});
}

antlrcpp::Any A::Analyzer::visitAId(LatteParser::AIdContext* ctx) {
    std::string name = ctx->ID()->getText();
    if (functions.find(name) != functions.end()) {
        return antlrcpp::Any(std::static_pointer_cast<LValue>(functions.at(name)));
    } else if (variables.find(name) != variables.end()){
        return antlrcpp::Any(std::static_pointer_cast<LValue>(variables.at(name)));
    } else {
        reportError(UNDEFINED_IDENTIFIER, ctx->getStart());
    }
}

antlrcpp::Any A::Analyzer::visitAMember(LatteParser::AMemberContext* ctx) {
    throw UnsupportedOperationException{"Analyzer::visitAMember"};
}

antlrcpp::Any A::Analyzer::visitAddOp(LatteParser::AddOpContext* ctx) {
    std::string operator_ = ctx->getText();
    if (operator_ == "+") {
        return antlrcpp::Any(ADD);
    } else if (operator_ == "-") {
        return antlrcpp::Any(SUB);
    } else {
        throw UnsupportedOperationException("Analyzer::visitAddOp unsupported operator");
    }
}

antlrcpp::Any A::Analyzer::visitMulOp(LatteParser::MulOpContext* ctx) {
    std::string operator_ = ctx->getText();
    if (operator_ == "*") {
        return antlrcpp::Any(MUL);
    } else if (operator_ == "/") {
        return antlrcpp::Any(DIV);
    } else if (operator_ == "%") {
        return antlrcpp::Any(MOD);
    } else {
        throw UnsupportedOperationException("Analyzer::visitMulOp unsupported operator");
    }
}

antlrcpp::Any A::Analyzer::visitRelOp(LatteParser::RelOpContext* ctx) {
    std::string operator_ = ctx->getText();
    if (operator_ == "==") {
        return antlrcpp::Any(EQ);
    } else if (operator_ == "!=") {
        return antlrcpp::Any(NE);
    } else if (operator_ == "<") {
        return antlrcpp::Any(LT);
    } else if (operator_ == "<=") {
        return antlrcpp::Any(LE);
    } else if (operator_ == ">") {
        return antlrcpp::Any(GT);
    } else if (operator_ == ">=") {
        return antlrcpp::Any(GE);
    } else {
        throw UnsupportedOperationException("Analyzer::visitRelOp unsupported operator");
    }
}

std::string typeToString(Type type) {
    switch (type) {
        case INT:
            return "int";
        case STR:
            return "str";
        case BOOLEAN:
            return "bool";
        case OBJECT:
            return "object";
        case VOID:
            return "void";
    }
}

void A::Analyzer::reportError(ErrorType error, antlr4::Token* token) {
    int lineNumber = token->getLine();
    int positionInLine = token->getCharPositionInLine();

    std::string message;
    switch(error) {
        case INVALID_NUMBER_OF_ARGUMENTS:
            message = "Invalid number of arguments in function call";
            break;
        case INVALID_ARGUMENT_TYPE:
            message = "Invalid argument type in function call";
            break;
        case ATTEMPT_TO_USE_FUNCTION_AS_VARIABLE:
            message = "Attempt to use function as variable";
            break;
        case ATTEMPT_TO_USE_VARIABLE_AS_FUNCTION:
            message = "Attempts to use variable as function";
            break;
        case MISSING_MAIN:
            message = "No main function";
            break;
        case DUPLICATE_ARGUMENT_NAME:
            message = "Duplicate argument name";
            break;
        case REDECLARATION_OF_VARIABLE:
            message = "Redeclaration of variable";
            break;
        case UNDEFINED_IDENTIFIER:
            message = "Undefined identifier";
            break;
        case INTEGER_OUT_OF_RANGE:
            message = "Integer constant out of range";
            break;
        case DUPLICATE_FUNCTION_NAME:
            message = "Duplicate function name";
            break;
        case FUNCTION_DOES_NOT_RETURN:
            message = "At least one the branches of function does not return";
            break;
        case FUNCTION_RETURNS_WRONG_TYPE:
            message = "Function returns invalid type";
            break;
    }
    std::string completeMessage = "ERROR, " + message + " at " + std::to_string(lineNumber) + ":" + std::to_string(positionInLine);

    throw Error{completeMessage, lineNumber, positionInLine};
}

void A::Analyzer::reportInvalidExpressionTypeError(Type expectedType, Type actualType, antlr4::Token* token) {
    int lineNumber = token->getLine();
    int positionInLine = token->getCharPositionInLine();
    std::string completeMessage = "ERROR, invalid expression type " + typeToString(actualType) + ", expected "
        + typeToString(expectedType) + " at " + std::to_string(lineNumber) + ":" + std::to_string(positionInLine);

    throw Error{completeMessage, lineNumber, positionInLine};
}

void A::Analyzer::emplacePredefinedFunctions() {
    std::shared_ptr<Function> printInt = std::make_shared<Function>(
        VOID, std::vector<Argument>{Argument{"whatever", INT}}, "printInt");
    functions.insert(std::make_pair("printInt", printInt));

    std::shared_ptr<Function> printString = std::make_shared<Function>(
        VOID, std::vector<Argument>{Argument{"whatever", STR}}, "printString");
    functions.insert(std::make_pair("printString", printString));

    std::shared_ptr<Function> readString = std::make_shared<Function>(STR, std::vector<Argument>{}, "readString");
    functions.insert(std::make_pair("readString", readString));

    std::shared_ptr<Function> readInt = std::make_shared<Function>(INT, std::vector<Argument>{}, "readInt");
    functions.insert(std::make_pair("readInt", readInt));
}

std::shared_ptr<Value> A::Analyzer::unknown(Type type) {
    switch (type) {
        case INT:
            return Integer::unknown();
        case STR:
            return Str::unknown();
        case BOOLEAN:
            return Boolean::unknown();
        case OBJECT:
            throw UnsupportedOperationException{"wtf"};
        case VOID:
            throw InvalidTypeException{};
    }
}

std::shared_ptr<Value> A::Analyzer::nullOf(Type type) {
    switch (type) {
        case INT:
            return Integer::default_();
        case STR:
            return Str::default_();
        case BOOLEAN:
            return Boolean::default_();
        case OBJECT:
            throw UnsupportedOperationException{"wtf"};
        case VOID:
            throw InvalidTypeException{};
    }
}
