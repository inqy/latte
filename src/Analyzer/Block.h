#ifndef LATTE_BLOCK_H
#define LATTE_BLOCK_H

namespace A {
    class Block {
    public:
        Block(const bool returned, const Type returnType) : returned(returned), returnType(returnType) {}
        const bool returned;
        const Type returnType;
    };
}

#endif //LATTE_BLOCK_H
