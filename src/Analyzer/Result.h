#ifndef LATTE_RESULT_H
#define LATTE_RESULT_H

#include <utility>
#include <vector>
#include "Error.h"

namespace A {
    class Result {
    public:
        Result() : errors() {}

        explicit Result(std::vector<Error> errors) : errors(std::move(errors)) {}

        const std::vector<A::Error> errors;
    };
}

#endif //LATTE_RESULT_H
