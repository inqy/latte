#ifndef LATTE_ANALYZER_H
#define LATTE_ANALYZER_H

#include "Error.h"
#include "../../generated/LatteBaseVisitor.h"
#include "Variable.h"
#include "Function.h"
#include "../ErrorType.h"

#include <memory>

namespace A {
    class Analyzer : public LatteBaseVisitor {
    public:
        using Variables = std::map<std::string, std::shared_ptr<Variable>>;
        using Functions = std::map<std::string, std::shared_ptr<Function>>;

        antlrcpp::Any visitProgram(LatteParser::ProgramContext* context) override;
        antlrcpp::Any visitTopDef(LatteParser::TopDefContext* context) override;
        antlrcpp::Any visitArg(LatteParser::ArgContext* context) override;
        antlrcpp::Any visitBlock(LatteParser::BlockContext* ctx) override;
        antlrcpp::Any visitEmpty(LatteParser::EmptyContext* ctx) override;
        antlrcpp::Any visitBlockStmt(LatteParser::BlockStmtContext* ctx) override;
        antlrcpp::Any visitDecl(LatteParser::DeclContext* ctx) override;
        antlrcpp::Any visitAss(LatteParser::AssContext* ctx) override;
        antlrcpp::Any visitIncr(LatteParser::IncrContext* ctx) override;
        antlrcpp::Any visitDecr(LatteParser::DecrContext* ctx) override;
        antlrcpp::Any visitRet(LatteParser::RetContext* ctx) override;
        antlrcpp::Any visitVRet(LatteParser::VRetContext* ctx) override;
        antlrcpp::Any visitCond(LatteParser::CondContext* ctx) override;
        antlrcpp::Any visitCondElse(LatteParser::CondElseContext* ctx) override;
        antlrcpp::Any visitWhile(LatteParser::WhileContext* ctx) override;
        antlrcpp::Any visitSExp(LatteParser::SExpContext* ctx) override;
        antlrcpp::Any visitInt(LatteParser::IntContext* ctx) override;
        antlrcpp::Any visitStr(LatteParser::StrContext* ctx) override;
        antlrcpp::Any visitBool(LatteParser::BoolContext* ctx) override;
        antlrcpp::Any visitVoid(LatteParser::VoidContext* ctx) override;
        antlrcpp::Any visitItem(LatteParser::ItemContext* ctx) override;
        antlrcpp::Any visitELvalue(LatteParser::ELvalueContext* ctx) override;
        antlrcpp::Any visitEFunCall(LatteParser::EFunCallContext* ctx) override;
        antlrcpp::Any visitERelOp(LatteParser::ERelOpContext* ctx) override;
        antlrcpp::Any visitETrue(LatteParser::ETrueContext* ctx) override;
        antlrcpp::Any visitEOr(LatteParser::EOrContext* ctx) override;
        antlrcpp::Any visitEInt(LatteParser::EIntContext* ctx) override;
        antlrcpp::Any visitEUnOp(LatteParser::EUnOpContext* ctx) override;
        antlrcpp::Any visitEStr(LatteParser::EStrContext* ctx) override;
        antlrcpp::Any visitEMulOp(LatteParser::EMulOpContext* ctx) override;
        antlrcpp::Any visitEAnd(LatteParser::EAndContext* ctx) override;
        antlrcpp::Any visitEParen(LatteParser::EParenContext* ctx) override;
        antlrcpp::Any visitEFalse(LatteParser::EFalseContext* ctx) override;
        antlrcpp::Any visitEAddOp(LatteParser::EAddOpContext* ctx) override;
        antlrcpp::Any visitAId(LatteParser::AIdContext* ctx) override;
        antlrcpp::Any visitAMember(LatteParser::AMemberContext* ctx) override;
        antlrcpp::Any visitAddOp(LatteParser::AddOpContext* ctx) override;
        antlrcpp::Any visitMulOp(LatteParser::MulOpContext* ctx) override;
        antlrcpp::Any visitRelOp(LatteParser::RelOpContext* ctx) override;

    private:
        void emplacePredefinedFunctions();
        static void reportError(ErrorType error, antlr4::Token* token);
        static void reportInvalidExpressionTypeError(Type expectedType, Type actualType, antlr4::Token* token);
        static std::shared_ptr<Value> unknown(Type type);
        static std::shared_ptr<Value> nullOf(Type type);

        // Global state:
        bool gatherTopDefsMode = true;
        std::vector<Error> errors{};
        Functions functions;

        // Local state:
        size_t depth;
        Variables variables;
        Type currentFuncReturnType;
    };
}

#endif //LATTE_ANALYZER_H
