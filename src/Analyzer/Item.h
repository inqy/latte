#ifndef LATTE_ITEM_H
#define LATTE_ITEM_H

#include <string>
#include <utility>
#include "../Value/Value.h"

namespace A {
    class Item {
    public:
        Item(std::string name, std::shared_ptr<Value> value) : name(std::move(name)), value(std::move(value)) {}

        const std::string name;
        std::shared_ptr<Value> value;
    };
}

#endif //LATTE_ITEM_H
