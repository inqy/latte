#ifndef LATTE_LVALUE_H
#define LATTE_LVALUE_H

#include <string>

namespace A {
    static const char* const LVALUE_FUNC = "func";
    static const char* const LVALUE_VAR = "var";

    class LValue {
    public:
        virtual std::string tIdent() = 0;
    };
}

#endif //LATTE_LVALUE_H
