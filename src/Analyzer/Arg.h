#ifndef LATTE_ARG_H
#define LATTE_ARG_H

#include <utility>

#include "Variable.h"
#include "Function.h"

namespace A {
    class Arg {
    public:
        Arg(std::vector<Argument> arguments) : arguments(std::move(arguments)) {}

        const std::vector<Argument> arguments;
    };
}

#endif //LATTE_ARG_H
