#ifndef LATTE_VARIABLE_H
#define LATTE_VARIABLE_H

#include <string>
#include <utility>
#include "LValue.h"
#include "../Value/Value.h"

namespace A {
    class Variable : public A::LValue {
    public:
        Variable(std::string name, Type type, std::shared_ptr<Value> value, size_t depth) : name(std::move(name)),
                                                                                            type(type),
                                                                                            value(std::move(value)), depth(depth) {}

        std::string tIdent() override {
            return A::LVALUE_VAR;
        }

        const size_t depth;
        const std::string name;
        const Type type;
        std::shared_ptr<Value> value;
    };
}

#endif //LATTE_VARIABLE_H
