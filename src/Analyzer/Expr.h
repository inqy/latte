#ifndef LATTE_EXPR_H
#define LATTE_EXPR_H

#include <string>
#include <utility>

namespace A {
    class Expr {
    public:
        explicit Expr(const std::shared_ptr<Value>& value) : value(value) {}

        const std::shared_ptr<Value> value;
    };
}

#endif //LATTE_EXPR_H
