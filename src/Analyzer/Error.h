#ifndef LATTE_ERROR_H
#define LATTE_ERROR_H

#include <string>
#include <utility>
#include <stdexcept>

namespace A {
    class Error : public std::runtime_error {
    public:
        Error(const std::string& arg, const int lineNumber, const int positionInLine) : runtime_error(arg), lineNumber(lineNumber),
                                                                                        positionInLine(positionInLine) {}

        const int lineNumber;
        const int positionInLine;
    };
}

#endif //LATTE_ERROR_H
