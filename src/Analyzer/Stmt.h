#ifndef LATTE_STMT_H
#define LATTE_STMT_H

namespace A {
    class Stmt {
    public:
        Stmt() : returned(false),
                 returnType(VOID) {};

        explicit Stmt(Type returnType) : returned(true),
                                         returnType(returnType) {};

        bool returned;
        Type returnType;
    };
}

#endif //LATTE_STMT_H
