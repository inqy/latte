#ifndef LATTE_FUNCTION_H
#define LATTE_FUNCTION_H

#include "LValue.h"
#include "../Value/Value.h"

#include <string>
#include <utility>
#include <vector>

namespace A {
    class Argument {
    public:
        Argument(std::string name, Type type) : name(std::move(name)), type(type) {}

        std::string name;
        Type type;
    };

    class Function : public LValue {
    public:
        Function(const Type type, std::vector<Argument> args, std::string name)
            : type(type), args(std::move(args)), name(std::move(name)) {}

        std::string tIdent() override {
            return LVALUE_FUNC;
        }

        const Type type;
        const std::vector<Argument> args;
        const std::string name;
        // TODO: class
    };
}

#endif //LATTE_FUNCTION_H
