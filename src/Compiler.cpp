#include "Compiler.h"
#include "Analyzer/Result.h"
#include "Analyzer/Error.h"
#include "../generated/LatteLexer.h"
#include "../generated/LatteParser.h"
#include "Analyzer/Analyzer.h"
#include "Analyzer/Error.h"
#include "Analyzer/Result.h"
#include "Analyzer/Analyzer.h"
#include "Tree/Builder.h"
#include "Generator/Generator.h"
#include "Value/Int.h"
#include "Value/Str.h"
#include "PrettyPrinter.h"
#include "Generator/Assembler.h"
#include <vector>
#include <string>
#include <iostream>
#include <ANTLRInputStream.h>

std::string compileFile(const std::string& filepath) {
    using namespace antlr4;
    std::ifstream stream;
    stream.open(filepath);

    ANTLRInputStream input(stream);
    LatteLexer lexer(&input);
    unsigned int nLexerErrors = lexer.getNumberOfSyntaxErrors();
    if (nLexerErrors > 0) {
        A::Result result{};
        return "";
    }

    CommonTokenStream tokens(&lexer);
    LatteParser parser(&tokens);

    unsigned int nParserErrors = parser.getNumberOfSyntaxErrors();
    if (nParserErrors > 0) {
        A::Result result{};
        return "";
    }

    LatteParser::ProgramContext* tree = parser.program();
    A::Analyzer visitor;
    T::Builder builder;
    auto result = visitor.visitProgram(tree).as<A::Result>();
    auto bTree = builder.visitProgram(tree).as<std::shared_ptr<T::Program>>();
    G::Generator generator;
    std::vector<std::shared_ptr<G::Operation>> code = generator.generate(bTree);
    G::Assembler assembler;
    std::string asm_ = assembler.assemble(code, generator.getStrings());
    return asm_;
}


int Compiler::compile(std::string filepath) {
    std::ifstream sourceFile{filepath};
    std::vector<std::string> fileLines;
    if (sourceFile.is_open()) {
        std::string line;
        while (getline(sourceFile, line)) {
            fileLines.push_back(line);
        }
        sourceFile.close();
    }
    sourceFile.close();

    try {
        std::string result = compileFile(filepath);
        std::cout << "OK\n";
        std::string outpath = filepath.replace(filepath.size() - 4, std::string::npos, ".s");
        std::ofstream outputFile{outpath};
        outputFile << result;
        outputFile.close();
    } catch(const A::Error& e) {
        std::cout << "ERROR\n";
        std::cout << e.what() << std::endl;
        std::cout << "At: " << fileLines[e.lineNumber - 1] << std::endl << std::string(e.positionInLine + 4, ' ') << "^\n";
        return 1;
    }

    return 0;
}
