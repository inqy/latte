#include <vector>
#include <iostream>
#include "PrettyPrinter.h"
#include "Generator/Data.h"
#include "Value/Int.h"
#include "Value/Str.h"
#include "Generator/Operation.h"

void PrettyPrinter::printData(std::shared_ptr<G::Data> ptr) {
    switch (ptr->type_()) {
        case G::REGISTER: {
            auto reg = std::static_pointer_cast<G::Register>(ptr);
            std::cout << "%" << reg->num << " ";
            break;
        }
        case G::CONST: {
            auto cnst = std::static_pointer_cast<G::Const>(ptr);
            switch (cnst->value->type()) {
                case INT: {
                    auto val = std::static_pointer_cast<Integer>(cnst->value);
                    std::cout << val->value << " ";
                    break;
                }
                case STR:{
                    auto val = std::static_pointer_cast<Str>(cnst->value);
                    std::cout << val->value;
                    break;
                }
                case BOOLEAN: {
                    auto val = std::static_pointer_cast<Boolean>(cnst->value);
                    std::cout << (val->value ? "true" : "false") << " ";
                    break;
                }
                case OBJECT:
                case VOID:
                    throw std::runtime_error{"wtf2"};
            }
            break;
        }
    }
}

void PrettyPrinter::printCode(std::vector<std::shared_ptr<G::Operation>> vector) {
    for (auto& op : vector) {
        switch(op->type_()) {
            case G::BIOP: {
                std::shared_ptr<G::BiOperation> bop = std::static_pointer_cast<G::BiOperation>(op);
                printData(bop->target);
                std::cout << "= ";
                printData(bop->left);
                switch (bop->operator_) {
                    case T::ADD:
                        std::cout << " + ";
                        break;
                    case T::SUB:
                        std::cout << " - ";
                        break;
                    case T::MUL:
                        std::cout << " * ";
                        break;
                    case T::DIV:
                        std::cout << " / ";
                        break;
                    case T::MOD:
                        std::cout << " % ";
                        break;
                    case T::AND:
                        std::cout << " && ";
                        break;
                    case T::OR:
                        std::cout << " || ";
                        break;
                    case T::EQ:
                        std::cout << " == ";
                        break;
                    case T::NE:
                        std::cout << " != ";
                        break;
                    case T::LT:
                        std::cout << " < ";
                        break;
                    case T::LE:
                        std::cout << " <= ";
                        break;
                    case T::GT:
                        std::cout << " > ";
                        break;
                    case T::GE:
                        std::cout << " >= ";
                        break;
                }
                printData(bop->right);
                std::cout << std::endl;
                break;
            }
            case G::UNOP: {
                std::shared_ptr<G::UnOperation> bop = std::static_pointer_cast<G::UnOperation>(op);
                printData(bop->target);
                switch (bop->operator_) {
                    case T::NOT:
                        std::cout << "= !";
                        break;
                    case T::NEG:
                        std::cout << "= -";
                        break;
                }
                printData(bop->source);
                std::cout << std::endl;
                break;
            }
            case G::COPY: {
                std::shared_ptr<G::Copy> bop = std::static_pointer_cast<G::Copy>(op);
                printData(bop->target);
                std::cout << "= ";
                printData(bop->source);
                std::cout << std::endl;
                break;
            }
            case G::JUMP: {
                std::shared_ptr<G::Jump> bop = std::static_pointer_cast<G::Jump>(op);
                std::cout << "jump " << bop->target << std::endl;
                break;
            }
            case G::LABEL: {
                std::shared_ptr<G::Label> bop = std::static_pointer_cast<G::Label>(op);
                std::cout << ".L" << bop->index << ":" << std::endl;
                break;
            }
            case G::CONDJUMP:{
                std::shared_ptr<G::CondJump> bop = std::static_pointer_cast<G::CondJump>(op);
                std::cout << "jump " << bop->target << " if ??" << std::endl;
                break;
            }
            case G::PARAM:{
                std::shared_ptr<G::Param> bop = std::static_pointer_cast<G::Param>(op);
                std::cout << "param ";
                printData(bop->source);
                std::cout << std::endl;
                break;
            }
            case G::CALL:{
                std::shared_ptr<G::Call> bop = std::static_pointer_cast<G::Call>(op);
                printData(bop->dest);
                std::cout << "= call " << bop->function << std::endl;
                break;
            }
            case G::RET:{
                std::shared_ptr<G::Ret> bop = std::static_pointer_cast<G::Ret>(op);
                std::cout << "return ";
                if (!bop->nothing) {
                    printData(bop->from);
                }
                std::cout << std::endl;
                break;
            }
            case G::FLABEL:
                std::shared_ptr<G::FuncLabel> bop = std::static_pointer_cast<G::FuncLabel>(op);
//                std::cout << ".func" << bop->index << ":" << std::endl;
                break;
        }
    }
}