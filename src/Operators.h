#ifndef LATTE_OPERATORS_H
#define LATTE_OPERATORS_H

enum AddOperator {
    ADD,
    SUB
};

enum MulOperator {
    MUL,
    DIV,
    MOD
};

enum RelationOperator {
    EQ,
    NE,
    GE,
    GT,
    LE,
    LT
};

#endif //LATTE_OPERATORS_H
