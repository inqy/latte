#include "Builder.h"

#include <utility>
#include "Function.h"
#include "Argument.h"
#include "Expr.h"
#include "../Value/Str.h"
#include "../Value/Int.h"
#include "Program.h"

antlrcpp::Any T::Builder::visitProgram(LatteParser::ProgramContext* ctx) {
    emplacePredefinedFunctions();
    std::vector<std::shared_ptr<Function>> fs;
    for (auto* topDef : ctx->topDef()) {
        visit(topDef);
    }
    topDefGatheringMode = false;
    for (auto* topDef : ctx->topDef()) {
        fs.push_back(visit(topDef).as<std::shared_ptr<Function>>());
    }
    std::shared_ptr<Program> prog = std::make_shared<Program>(fs);
    return antlrcpp::Any(prog);
}

antlrcpp::Any T::Builder::visitTopDef(LatteParser::TopDefContext* ctx) {
    const std::string& functionName = ctx->ID()->getText();
    Type type = visit(ctx->type_()).as<Type>();
    if (topDefGatheringMode) {
        std::vector<std::shared_ptr<Argument>> args{};
        if (ctx->arg() != nullptr) {
            args = visit(ctx->arg()).as<std::vector<std::shared_ptr<Argument>>>();
        }

        std::shared_ptr<Function> function = std::make_shared<Function>(functionName, args, type, 0, nullptr);
        functions[functionName] = function;
        return nullptr;
    }

    maxVariablesInFunction = 0;
    Variables parentVariables = variables;
    for (const auto& arg : functions[functionName]->args) {
        variables[arg->name] = arg->var;
    }

    auto block = visit(ctx->block()).as<std::shared_ptr<BlockStmt>>();
    functions[functionName]->block = block;
    functions[functionName]->variables = maxVariablesInFunction;
    variables = parentVariables;

    return antlrcpp::Any(functions[functionName]);
}

antlrcpp::Any T::Builder::visitArg(LatteParser::ArgContext* ctx) {
    std::vector<std::shared_ptr<Argument>> args;
    for (int i = 0; i < ctx->ID().size(); i++) {
        std::string id = ctx->ID(i)->getText();
        Type type = visit(ctx->type_(i));

        auto var = std::make_shared<Variable>(id, type);
        auto arg = std::make_shared<Argument>(id, type, var);

        args.push_back(arg);
    }

    return antlrcpp::Any(args);
}

antlrcpp::Any T::Builder::visitBlock(LatteParser::BlockContext* ctx) {
    Variables parentVariables = variables;
    VariableSet parentVariableSet = variableSet;

    Stmts v;
    for (auto* stmt : ctx->stmt()) {
        auto generatedStmt = visit(stmt).as<std::shared_ptr<Stmt>>();
        v.push_back(generatedStmt);
    }

    maxVariablesInFunction = std::max(maxVariablesInFunction, (int)variableSet.size());

    variables = parentVariables;
    variableSet = parentVariableSet;

    auto block = std::make_shared<BlockStmt>(v);
    return antlrcpp::Any(block);
}

antlrcpp::Any T::Builder::visitEmpty(LatteParser::EmptyContext* ctx) {
    std::shared_ptr<Stmt> empty = std::make_shared<Several>(Stmts{});
    return antlrcpp::Any(empty);
}

antlrcpp::Any T::Builder::visitBlockStmt(LatteParser::BlockStmtContext* ctx) {
    std::shared_ptr<Stmt> block = visit(ctx->block()).as<std::shared_ptr<BlockStmt>>();
    return antlrcpp::Any(block);
}

antlrcpp::Any T::Builder::visitDecl(LatteParser::DeclContext* ctx) {
    Type type = visit(ctx->type_()).as<Type>();
    currentDeclType = type;
    Stmts v;
    for (auto* item : ctx->item()) {
        auto itemR = visit(item).as<std::shared_ptr<Item>>();
        auto var = std::make_shared<Variable>(itemR->name, type);
        variables[itemR->name] = var;
        variableSet.insert(var);
        std::shared_ptr<Stmt> ass = std::make_shared<Ass>(var, itemR->expr);
        v.push_back(ass);
    }

    std::shared_ptr<Stmt> stmts = std::make_shared<Several>(v);
    return antlrcpp::Any(stmts);
}

antlrcpp::Any T::Builder::visitAss(LatteParser::AssContext* ctx) {
    auto var = visit(ctx->lvalue()).as<std::shared_ptr<Variable>>();
    auto value = visit(ctx->expr()).as<std::shared_ptr<Expr>>();
    std::shared_ptr<Stmt> ass = std::make_shared<Ass>(var, value);
    return antlrcpp::Any(ass);
}

antlrcpp::Any T::Builder::visitIncr(LatteParser::IncrContext* ctx) {
    auto var = visit(ctx->lvalue()).as<std::shared_ptr<Variable>>();
    std::shared_ptr<Stmt> incr = std::make_shared<Inc>(var);
    return antlrcpp::Any(incr);
}

antlrcpp::Any T::Builder::visitDecr(LatteParser::DecrContext* ctx) {
    auto var = visit(ctx->lvalue()).as<std::shared_ptr<Variable>>();
    std::shared_ptr<Stmt> decr = std::make_shared<Dec>(var);
    return antlrcpp::Any(decr);
}

antlrcpp::Any T::Builder::visitRet(LatteParser::RetContext* ctx) {
    auto expr = visit(ctx->expr()).as<std::shared_ptr<Expr>>();
    std::shared_ptr<Stmt> ret = std::make_shared<Return>(expr);
    return antlrcpp::Any(ret);
}

antlrcpp::Any T::Builder::visitVRet(LatteParser::VRetContext* ctx) {
    std::shared_ptr<Stmt> vret = std::make_shared<VoidReturn>();
    return antlrcpp::Any(vret);
}

antlrcpp::Any T::Builder::visitCond(LatteParser::CondContext* ctx) {
    auto expr = visit(ctx->expr()).as<std::shared_ptr<Expr>>();
    auto ifBody = visit(ctx->stmt()).as<std::shared_ptr<Stmt>>();
    std::shared_ptr<Stmt> if_ = std::make_shared<If>(expr, ifBody);
    return antlrcpp::Any(if_);
}

antlrcpp::Any T::Builder::visitCondElse(LatteParser::CondElseContext* ctx) {
    auto expr = visit(ctx->expr()).as<std::shared_ptr<Expr>>();
    auto thenBody = visit(ctx->stmt(0)).as<std::shared_ptr<Stmt>>();
    auto elseBody = visit(ctx->stmt(1)).as<std::shared_ptr<Stmt>>();
    std::shared_ptr<Stmt> if_ = std::make_shared<IfElse>(expr, thenBody, elseBody);
    return antlrcpp::Any(if_);
}

antlrcpp::Any T::Builder::visitWhile(LatteParser::WhileContext* ctx) {
    auto expr = visit(ctx->expr()).as<std::shared_ptr<Expr>>();
    auto stmt = visit(ctx->stmt()).as<std::shared_ptr<Stmt>>();
    std::shared_ptr<Stmt> while_ = std::make_shared<While>(expr, stmt);
    return antlrcpp::Any(while_);
}

antlrcpp::Any T::Builder::visitSExp(LatteParser::SExpContext* ctx) {
    auto expr = visit(ctx->expr()).as<std::shared_ptr<Expr>>();
    auto stmts = extractFunctionCalls(expr);
    std::shared_ptr<Stmt> several = std::make_shared<Several>(stmts);
    return antlrcpp::Any(several);
}

antlrcpp::Any T::Builder::visitInt(LatteParser::IntContext* ctx) {
    return antlrcpp::Any(INT);
}

antlrcpp::Any T::Builder::visitStr(LatteParser::StrContext* ctx) {
    return antlrcpp::Any(STR);
}

antlrcpp::Any T::Builder::visitBool(LatteParser::BoolContext* ctx) {
    return antlrcpp::Any(BOOLEAN);
}

antlrcpp::Any T::Builder::visitVoid(LatteParser::VoidContext* ctx) {
    return antlrcpp::Any(VOID);
}

antlrcpp::Any T::Builder::visitItem(LatteParser::ItemContext* ctx) {
    std::string name = ctx->ID()->getText();
    std::shared_ptr<Expr> expr;
    if (ctx->expr() != nullptr) {
        expr = visit(ctx->expr()).as<std::shared_ptr<Expr>>();
    } else {
        std::shared_ptr<Value> value = nullOf(currentDeclType);
        expr = std::make_shared<Const>(value);
    }
    std::shared_ptr<Item> item = std::make_shared<Item>(name, expr);
    return antlrcpp::Any(item);
}

antlrcpp::Any T::Builder::visitELvalue(LatteParser::ELvalueContext* ctx) {
    std::shared_ptr<Variable> var = visit(ctx->lvalue()).as<std::shared_ptr<Variable>>();
    std::shared_ptr<Expr> access = std::static_pointer_cast<Expr>(std::make_shared<VariableAccess>(var, var->type));
    return antlrcpp::Any(access);
}

antlrcpp::Any T::Builder::visitEFunCall(LatteParser::EFunCallContext* ctx) {
    std::shared_ptr<Function> func = visit(ctx->lvalue()).as<std::shared_ptr<Function>>();
    std::vector<std::shared_ptr<Expr>> args;
    for(int i = 0; i < ctx->expr().size(); i++) {
        std::shared_ptr<Expr> arg = visit(ctx->expr().at(i)).as<std::shared_ptr<Expr>>();
        args.push_back(arg);
    }

    std::shared_ptr<Expr> funcCall = std::static_pointer_cast<Expr>(std::make_shared<FuncCall>(args, func, func->type));
    return antlrcpp::Any(funcCall);
}

antlrcpp::Any T::Builder::visitERelOp(LatteParser::ERelOpContext* ctx) {
    std::shared_ptr<Expr> left = visit(ctx->expr().at(0)).as<std::shared_ptr<Expr>>();
    std::shared_ptr<Expr> right = visit(ctx->expr().at(1)).as<std::shared_ptr<Expr>>();

    BiOperatorType operator_ = visit(ctx->relOp());
    if (Const::isConst(left) && Const::isConst(right)) {
        auto leftConst = Const::toConst(left);
        auto rightConst = Const::toConst(right);

        std::shared_ptr<Value> result;
        switch(operator_) {
            case EQ:
                result = *(leftConst->value) == *(rightConst->value);
                break;
            case NE:
                result = *(leftConst->value) != *(rightConst->value);
                break;
            case GE:
                result = *(leftConst->value) >= *(rightConst->value);
                break;
            case GT:
                result = *(leftConst->value) > *(rightConst->value);
                break;
            case LE:
                result = *(leftConst->value) <= *(rightConst->value);
                break;
            case LT:
                result = *(leftConst->value) < *(rightConst->value);
                break;
            default:
                throw UnsupportedOperationException{"T::Builder::visitERelOp unknown operator"};
        }
        std::shared_ptr<Expr> expr = std::make_shared<Const>(result);
        return antlrcpp::Any(expr);
    } else {
        std::shared_ptr<Expr> expr = std::make_shared<BiOperator>(operator_, left, right, left->valueType());
        return antlrcpp::Any(expr);
    }
}

antlrcpp::Any T::Builder::visitETrue(LatteParser::ETrueContext* ctx) {
    std::shared_ptr<Value> value = std::make_shared<Boolean>(true);
    std::shared_ptr<Expr> expr = std::make_shared<Const>(value);
    return antlrcpp::Any(expr);
}

antlrcpp::Any T::Builder::visitEOr(LatteParser::EOrContext* ctx) {
    std::shared_ptr<Expr> left = visit(ctx->expr().at(0)).as<std::shared_ptr<Expr>>();
    std::shared_ptr<Expr> right = visit(ctx->expr().at(1)).as<std::shared_ptr<Expr>>();

    if (Const::isConst(left) && Const::isConst(right)) {
        auto leftConst = Const::toConst(left);
        auto rightConst = Const::toConst(right);

        std::shared_ptr<Value> result = *(leftConst->value) || *(rightConst->value);
        std::shared_ptr<Expr> expr = std::make_shared<Const>(result);
        return antlrcpp::Any(expr);
    } else {
        std::shared_ptr<Expr> expr = std::make_shared<BiOperator>(OR, left, right, left->valueType());
        return antlrcpp::Any(expr);
    }
}

antlrcpp::Any T::Builder::visitEInt(LatteParser::EIntContext* ctx) {
    int intValue = std::stoi(ctx->getText());
    std::shared_ptr<Value> value = std::make_shared<Integer>(intValue);
    std::shared_ptr<Expr> expr = std::make_shared<Const>(value);
    return antlrcpp::Any(expr);
}

antlrcpp::Any T::Builder::visitEUnOp(LatteParser::EUnOpContext* ctx) {
    std::shared_ptr<Expr> center = visit(ctx->expr()).as<std::shared_ptr<Expr>>();

    UnOperatorType operator_ = ctx->getText()[0] == '-' ? NEG : NOT;
    if (Const::isConst(center)) {
        auto centerConst = Const::toConst(center);

        std::shared_ptr<Value> result;
        switch(operator_) {
            case NOT:
                result = !(*(centerConst->value));
                break;
            case NEG:
                result = -(*(centerConst->value));
                break;
            default:
                throw UnsupportedOperationException{"T::Builder::visitEUnOp unknown operator"};
        }
        std::shared_ptr<Expr> expr = std::make_shared<Const>(result);
        return antlrcpp::Any(expr);
    } else {
        std::shared_ptr<Expr> expr = std::make_shared<UnOperator>(operator_, center);
        return antlrcpp::Any(expr);
    }
}

antlrcpp::Any T::Builder::visitEStr(LatteParser::EStrContext* ctx) {
    std::string text = ctx->getText();
    text = text.substr(1, ctx->getText().size() - 2);
    std::shared_ptr<Value> value = std::make_shared<Str>(text);
    std::shared_ptr<Expr> expr = std::make_shared<Const>(value);
    return antlrcpp::Any(expr);
}

antlrcpp::Any T::Builder::visitEMulOp(LatteParser::EMulOpContext* ctx) {
    std::shared_ptr<Expr> left = visit(ctx->expr().at(0)).as<std::shared_ptr<Expr>>();
    std::shared_ptr<Expr> right = visit(ctx->expr().at(1)).as<std::shared_ptr<Expr>>();

    BiOperatorType operator_ = visit(ctx->mulOp());
    if (Const::isConst(left) && Const::isConst(right)) {
        auto leftConst = Const::toConst(left);
        auto rightConst = Const::toConst(right);

        std::shared_ptr<Value> result;
        switch(operator_) {
            case MUL:
                result = *(leftConst->value) * *(rightConst->value);
                break;
            case DIV:
                result = *(leftConst->value) / *(rightConst->value);
                break;
            case MOD:
                result = *(leftConst->value) % *(rightConst->value);
                break;
            default:
                throw UnsupportedOperationException{"T::Builder::visitEMulOp unknown operator"};
        }
        std::shared_ptr<Expr> expr = std::make_shared<Const>(result);
        return antlrcpp::Any(expr);
    } else {
        std::shared_ptr<Expr> expr = std::make_shared<BiOperator>(operator_, left, right, left->valueType());
        return antlrcpp::Any(expr);
    }
}

antlrcpp::Any T::Builder::visitEAnd(LatteParser::EAndContext* ctx) {
    std::shared_ptr<Expr> left = visit(ctx->expr().at(0)).as<std::shared_ptr<Expr>>();
    std::shared_ptr<Expr> right = visit(ctx->expr().at(1)).as<std::shared_ptr<Expr>>();

    if (Const::isConst(left) && Const::isConst(right)) {
        auto leftConst = Const::toConst(left);
        auto rightConst = Const::toConst(right);

        std::shared_ptr<Value> result = *(leftConst->value) && *(rightConst->value);
        std::shared_ptr<Expr> expr = std::make_shared<Const>(result);
        return antlrcpp::Any(expr);
    } else {
        std::shared_ptr<Expr> expr = std::make_shared<BiOperator>(AND, left, right, left->valueType());
        return antlrcpp::Any(expr);
    }
}

antlrcpp::Any T::Builder::visitEParen(LatteParser::EParenContext* ctx) {
    return visit(ctx->expr());
}

antlrcpp::Any T::Builder::visitEFalse(LatteParser::EFalseContext* ctx) {
    std::shared_ptr<Value> value = std::make_shared<Boolean>(false);
    std::shared_ptr<Expr> expr = std::make_shared<Const>(value);
    return antlrcpp::Any(expr);
}

antlrcpp::Any T::Builder::visitEAddOp(LatteParser::EAddOpContext* ctx) {
    std::shared_ptr<Expr> left = visit(ctx->expr().at(0)).as<std::shared_ptr<Expr>>();
    std::shared_ptr<Expr> right = visit(ctx->expr().at(1)).as<std::shared_ptr<Expr>>();

    BiOperatorType operator_ = visit(ctx->addOp());
    if (Const::isConst(left) && Const::isConst(right)) {
        auto leftConst = Const::toConst(left);
        auto rightConst = Const::toConst(right);

        std::shared_ptr<Value> result;
        switch(operator_) {
            case ADD:
                result = *(leftConst->value) + *(rightConst->value);
                break;
            case SUB:
                result = *(leftConst->value) - *(rightConst->value);
                break;
            default:
                throw UnsupportedOperationException{"T::Builder::visitEAddOp unknown operator"};
        }
        std::shared_ptr<Expr> expr = std::make_shared<Const>(result);
        return antlrcpp::Any(expr);
    } else if (left->valueType() == STR && operator_ == ADD) {
        std::vector<std::shared_ptr<Expr>> args = {left, right};
        auto func = functions.at("concat");
        std::shared_ptr<Expr> funcCall = std::static_pointer_cast<Expr>(std::make_shared<FuncCall>(args, func, func->type));
        return antlrcpp::Any(funcCall);
    } else {
        std::shared_ptr<Expr> expr = std::make_shared<BiOperator>(operator_, left, right, left->valueType());
        return antlrcpp::Any(expr);
    }
}

antlrcpp::Any T::Builder::visitAId(LatteParser::AIdContext* ctx) {
    std::string name = ctx->ID()->getText();
    if (functions.find(name) != functions.end()) {
        return antlrcpp::Any(functions[name]);
    }
    return antlrcpp::Any(variables[name]);
}

antlrcpp::Any T::Builder::visitAMember(LatteParser::AMemberContext* ctx) {
    throw UnsupportedOperationException{"T::Builder::visitAMember"};
}

antlrcpp::Any T::Builder::visitAddOp(LatteParser::AddOpContext* ctx) {
    std::string operator_ = ctx->getText();
    if (operator_ == "+") {
        return antlrcpp::Any(ADD);
    } else if (operator_ == "-") {
        return antlrcpp::Any(SUB);
    } else {
        throw UnsupportedOperationException("T::Builder::visitAddOp unsupported operator");
    }
}

antlrcpp::Any T::Builder::visitMulOp(LatteParser::MulOpContext* ctx) {
    std::string operator_ = ctx->getText();
    if (operator_ == "*") {
        return antlrcpp::Any(MUL);
    } else if (operator_ == "/") {
        return antlrcpp::Any(DIV);
    } else if (operator_ == "%") {
        return antlrcpp::Any(MOD);
    } else {
        throw UnsupportedOperationException("T::Builder::visitMulOp unsupported operator");
    }
}

antlrcpp::Any T::Builder::visitRelOp(LatteParser::RelOpContext* ctx) {
    std::string operator_ = ctx->getText();
    if (operator_ == "==") {
        return antlrcpp::Any(EQ);
    } else if (operator_ == "!=") {
        return antlrcpp::Any(NE);
    } else if (operator_ == "<") {
        return antlrcpp::Any(LT);
    } else if (operator_ == "<=") {
        return antlrcpp::Any(LE);
    } else if (operator_ == ">") {
        return antlrcpp::Any(GT);
    } else if (operator_ == ">=") {
        return antlrcpp::Any(GE);
    } else {
        throw UnsupportedOperationException("T::Builder::visitRelOp unsupported operator");
    }
}

std::shared_ptr<Value> T::Builder::nullOf(Type type) {
    switch (type) {
        case INT:
            return Integer::default_();
        case STR:
            return Str::default_();
        case BOOLEAN:
            return Boolean::default_();
        case OBJECT:
            throw UnsupportedOperationException{"wtf"};
        case VOID:
            throw InvalidTypeException{};
    }
}

std::vector<std::shared_ptr<T::Stmt>> T::Builder::extractFunctionCalls(const std::shared_ptr<Expr>& expr) {
    switch (expr->exprType()) {
        case UOp: {
            std::shared_ptr<UnOperator> ptr = std::static_pointer_cast<UnOperator>(expr);
            return extractFunctionCalls(ptr->expr);
        }
        case BOp: {
            std::shared_ptr<BiOperator> ptr = std::static_pointer_cast<BiOperator>(expr);
            auto vLeft = extractFunctionCalls(ptr->left);
            auto vRight = extractFunctionCalls(ptr->right);
            vLeft.insert(vLeft.end(), vRight.begin(), vRight.end());
        }
        case Con:
            return {};
        case FCall: {
            auto funcCall = FuncCall::toFuncCall(expr);
            return Stmts{std::make_shared<StmtFunc>(*funcCall)};
        }
        case VAcc:
            return {};
    }
}

void T::Builder::emplacePredefinedFunctions() {
    using Args = std::vector<std::shared_ptr<Argument>>;
    auto emptyBlock = std::make_shared<BlockStmt>(Stmts{});

    Args printIntArgs = {std::make_shared<Argument>("x", INT, nullptr)};
    auto printInt = std::make_shared<Function>("printInt", printIntArgs, VOID, 0, emptyBlock);
    functions.insert(std::make_pair("printInt", printInt));

    Args printStringArgs = {std::make_shared<Argument>("x", STR, nullptr)};
    auto printString = std::make_shared<Function>("printString", printStringArgs, VOID, 0, emptyBlock);
    functions.insert(std::make_pair("printString", printString));

    auto readString = std::make_shared<Function>("readString", Args{}, STR, 0, emptyBlock);
    functions.insert(std::make_pair("readString", readString));

    auto readInt = std::make_shared<Function>("readInt", Args{}, INT, 0, emptyBlock);
    functions.insert(std::make_pair("readInt", readInt));

    Args concatArgs = {std::make_shared<Argument>("x", STR, nullptr), std::make_shared<Argument>("y", STR, nullptr)};
    auto concat = std::make_shared<Function>("concat", concatArgs, STR, 0, emptyBlock);
    functions.insert(std::make_pair("concat", concat));
}