#ifndef LATTE_T_PROGRAM_H
#define LATTE_T_PROGRAM_H

#include <utility>
#include <vector>
#include "Function.h"

namespace T {
    class Program {
    public:
        explicit Program(std::vector<std::shared_ptr<Function>> functions) : functions(std::move(functions)) {}

        std::vector<std::shared_ptr<Function>> functions;
    };
};

#endif //LATTE_T_PROGRAM_H
