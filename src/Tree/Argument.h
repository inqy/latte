#ifndef LATTE_ARGUMENT_H
#define LATTE_ARGUMENT_H

#include <string>
#include <utility>
#include "../Value/Value.h"
#include "Variable.h"

namespace T {
    class Argument {
    public:
        Argument(const std::string& name, Type type, const std::shared_ptr<Variable>& var) : name(name), type(type), var(var) {}

        std::string name;
        Type type;
        std::shared_ptr<Variable> var;
    };
}
#endif //LATTE_ARGUMENT_H
