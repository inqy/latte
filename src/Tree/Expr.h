#ifndef LATTE_T_EXPR_H
#define LATTE_T_EXPR_H

#include "Variable.h"
#include "../Value/Value.h"
#include <utility>

namespace T {
    class Function;

    enum ExprType {
        UOp,
        BOp,
        Con,
        FCall,
        VAcc
    };

    class Expr {
    public:
        virtual ExprType exprType() = 0;
        virtual Type valueType() = 0;
    };

    enum UnOperatorType {
        NOT,
        NEG
    };

    class UnOperator : public Expr {
    public:
        UnOperator(UnOperatorType type, std::shared_ptr<Expr> expr) : type(type), expr(std::move(expr)) {}

        ExprType exprType() override {
            return UOp;
        }

        Type valueType() override {
            return expr->valueType();
        }

        UnOperatorType type;
        std::shared_ptr<Expr> expr;
    };

    enum BiOperatorType {
        ADD,
        SUB,
        MUL,
        DIV,
        MOD,
        AND,
        OR,
        EQ,
        NE,
        LT,
        LE,
        GT,
        GE
    };

    class BiOperator : public Expr {
    public:
        BiOperator(BiOperatorType type, std::shared_ptr<Expr> left, std::shared_ptr<Expr> right, Type valueType) : type(type),
                                                                                                                   left(std::move(left)),
                                                                                                                   right(std::move(right)),
                                                                                                                   valueType_(valueType) {}

        ExprType exprType() override {
            return BOp;
        }

        Type valueType() override {
            return valueType_;
        }

        BiOperatorType type;
        std::shared_ptr<Expr> left;
        std::shared_ptr<Expr> right;
        Type valueType_;
    };

    class Const : public Expr {
    public:
        static bool isConst(std::shared_ptr<Expr> ptr) {
            auto x = std::dynamic_pointer_cast<Const>(ptr);
            return (bool) x;
        }

        static std::shared_ptr<Const> toConst(const std::shared_ptr<Expr>& ptr) {
            return std::static_pointer_cast<Const>(ptr);
        }

        explicit Const(std::shared_ptr<Value> value) : value(std::move(value)) {}

        ExprType exprType() override {
            return Con;
        }

        Type valueType() override {
            return value->type();
        }

        std::shared_ptr<Value> value;
    };

    class FuncCall : public Expr {
    public:
        FuncCall(std::vector<std::shared_ptr<Expr>> args, std::shared_ptr<Function> function, Type type) : args(std::move(args)),
                                                                                                           function(std::move(function)),
                                                                                                           valueType_(type) {}

        ExprType exprType() override {
            return FCall;
        }

        Type valueType() override {
            return valueType_;
        }

        std::vector<std::shared_ptr<Expr>> args;
        std::shared_ptr<Function> function;
        Type valueType_;

        static bool isFuncCall(const std::shared_ptr<Expr>& ptr) {
            return (bool) (std::dynamic_pointer_cast<FuncCall>(ptr));
        }

        static std::shared_ptr<FuncCall> toFuncCall(const std::shared_ptr<Expr>& ptr) {
            return std::static_pointer_cast<FuncCall>(ptr);
        }
    };

    class VariableAccess : public Expr {
    public:
        explicit VariableAccess(std::shared_ptr<Variable> variable, Type type) : variable(std::move(variable)), valueType_(type) {}

        ExprType exprType() override {
            return VAcc;
        }

        Type valueType() override {
            return valueType_;
        }

        std::shared_ptr<Variable> variable;
        Type valueType_;
    };
}

#endif //LATTE_T_EXPR_H
