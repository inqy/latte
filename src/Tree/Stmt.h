#ifndef LATTE_T_STMT_H
#define LATTE_T_STMT_H

#include <utility>

#include "Expr.h"

namespace T {

    enum StmtType {
        BLK,
        FUN,
        WHI,
        IF,
        IFE,
        VRET,
        RET,
        DEC,
        INC,
        ASS,
        SEV
    };

    class Stmt {
    public:
        virtual StmtType type_() = 0;

        static std::string toString(StmtType type) {
            switch (type) {
                case FUN:
                    return "FUN";
                case WHI:
                    return "WHI";
                case IF:
                    return "IF";
                case IFE:
                    return "IFE";
                case VRET:
                    return "VRET";
                case RET:
                    return "RET";
                case DEC:
                    return "DEC";
                case INC:
                    return "INC";
                case ASS:
                    return "ASS";
                case BLK:
                    return "BLK";
                case SEV:
                    return "SEV";
            }
        }
    };

    using Stmts = std::vector<std::shared_ptr<Stmt>>;

    class BlockStmt : public Stmt {
    public:
        BlockStmt(std::vector<std::shared_ptr<T::Stmt>> stmts) : stmts(std::move(stmts)) {}

        StmtType type_() override {
            return BLK;
        }

        std::vector<std::shared_ptr<T::Stmt>> stmts;
    };

    class StmtFunc : public Stmt {
    public:
        explicit StmtFunc(const FuncCall& c) : args(c.args), function(c.function) {}

        static std::shared_ptr<StmtFunc> fromFuncCall(const std::shared_ptr<FuncCall>& ptr) {
            return std::make_shared<StmtFunc>(*ptr);
        }

        StmtType type_() override {
            return FUN;
        }

        std::vector<std::shared_ptr<Expr>> args;
        std::shared_ptr<Function> function;
    };

    class While : public Stmt {
    public:
        While(std::shared_ptr<Expr> condition, std::shared_ptr<Stmt> body) : condition(std::move(condition)), body(std::move(body)) {}

        StmtType type_() override {
            return WHI;
        }

        std::shared_ptr<Expr> condition;
        std::shared_ptr<Stmt> body;
    };

    class If : public Stmt {
    public:
        If(std::shared_ptr<Expr> condition, std::shared_ptr<Stmt> body) : condition(std::move(condition)), body(std::move(body)) {}

        StmtType type_() override {
            return IF;
        }

        std::shared_ptr<Expr> condition;
        std::shared_ptr<Stmt> body;
    };

    class IfElse : public Stmt {
    public:
        IfElse(std::shared_ptr<Expr> condition, std::shared_ptr<Stmt> thenBody, std::shared_ptr<Stmt> elseBody)
            : condition(std::move(condition)), thenBody(std::move(thenBody)), elseBody(std::move(elseBody)) {}

        StmtType type_() override {
            return IFE;
        }

        std::shared_ptr<Expr> condition;
        std::shared_ptr<Stmt> thenBody;
        std::shared_ptr<Stmt> elseBody;
    };

    class VoidReturn : public Stmt {
    public:
        StmtType type_() override {
            return VRET;
        }
    };

    class Return : public Stmt {
    public:
        explicit Return(std::shared_ptr<Expr> expr) : expr(std::move(expr)) {}

        StmtType type_() override {
            return RET;
        }

        std::shared_ptr<Expr> expr;
    };

    class Dec : public Stmt {
    public:
        explicit Dec(std::shared_ptr<Variable> var) : var(std::move(var)) {}

        StmtType type_() override {
            return DEC;
        }

        std::shared_ptr<Variable> var;
    };

    class Inc : public Stmt {
    public:
        explicit Inc(std::shared_ptr<Variable> var) : var(std::move(var)) {}

        StmtType type_() override {
            return INC;
        }

        std::shared_ptr<Variable> var;
    };

    class Ass : public Stmt {
    public:
        Ass(std::shared_ptr<Variable> var, std::shared_ptr<Expr> value) : var(std::move(var)), value(std::move(value)) {}

        StmtType type_() override {
            return ASS;
        }

        std::shared_ptr<Variable> var;
        std::shared_ptr<Expr> value;
    };

    class Several : public Stmt {
    public:
        Several(std::vector<std::shared_ptr<Stmt>> stmts) : stmts(std::move(stmts)) {}

        StmtType type_() override {
            return SEV;
        }

        std::vector<std::shared_ptr<Stmt>> stmts;
    };

    class Item {
    public:
        Item(std::string name, std::shared_ptr<Expr> expr) : name(std::move(name)), expr(std::move(expr)) {}

        std::string name;
        std::shared_ptr<Expr> expr;
    };
}

#endif //LATTE_T_STMT_H
