#ifndef LATTE_TT_FUNCTION_H
#define LATTE_TT_FUNCTION_H

#include "Argument.h"
#include "../Value/Value.h"
#include "Stmt.h"

#include <memory>
#include <utility>
#include <vector>

namespace T {
    class Function {
    public:
        Function(std::string name, std::vector<std::shared_ptr<Argument>>  args, Type type, int variables,
                 std::shared_ptr<BlockStmt> block) : name(std::move(name)), args(std::move(args)), type(type), variables(variables), block(std::move(block)) {}

        std::string name;
        std::vector<std::shared_ptr<Argument>> args;
        Type type;
        int variables;
        std::shared_ptr<BlockStmt> block;
    };
};

#endif //LATTE_TT_FUNCTION_H