#ifndef LATTE_T_VARIABLE_H
#define LATTE_T_VARIABLE_H

#include <utility>

#include "../Value/Value.h"

namespace T {
    class Variable {
    public:
        Variable(std::string name, Type type) : name(std::move(name)), type(type) {}

        std::string name;
        Type type;
    };
}

#endif //LATTE_T_VARIABLE_H
