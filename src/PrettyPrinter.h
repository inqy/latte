#ifndef LATTE_PRETTYPRINTER_H
#define LATTE_PRETTYPRINTER_H

#include "Generator/Data.h"
#include "Generator/Operation.h"

class PrettyPrinter {
public:
    static void printData(std::shared_ptr<G::Data> ptr);
    static void printCode(std::vector<std::shared_ptr<G::Operation>> vector);
};

#endif //LATTE_PRETTYPRINTER_H
