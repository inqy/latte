#ifndef LATTE_INT_H
#define LATTE_INT_H

#include <bits/shared_ptr.h>
#include "Value.h"
#include "Boolean.h"

class Integer : public Value {
public:
    explicit Integer(const int value) : value(value), _unknown(false) {}
    explicit Integer() : value(0), _unknown(true) {}

    [[nodiscard]] bool isUnknown() const override {
        return _unknown;
    }

    static std::shared_ptr<Value> default_() {
        return std::static_pointer_cast<Value>(std::make_shared<Integer>(0));
    }

    static std::shared_ptr<Value> unknown() {
        return std::static_pointer_cast<Value>(std::make_shared<Integer>());
    }

    std::shared_ptr<Value> operator==(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value == value));
    }

    std::shared_ptr<Value> operator!=(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value != value));
    }

    std::shared_ptr<Value> operator>(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>( value > otherCast->value));
    }

    std::shared_ptr<Value> operator<(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>( value < otherCast->value));
    }

    std::shared_ptr<Value> operator>=(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(value >= otherCast->value ));
    }

    std::shared_ptr<Value> operator<=(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>( value <= otherCast->value));
    }

    std::shared_ptr<Value> operator+(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Integer::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Integer>(otherCast->value + value));
    }

    std::shared_ptr<Value> operator-(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Integer::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Integer>( value - otherCast->value));
    }

    std::shared_ptr<Value> operator||(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator&&(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator*(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Integer::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Integer>(otherCast->value * value));
    }

    std::shared_ptr<Value> operator/(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Integer::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        if (otherCast->value == 0) {
            // TODO: div by 0
        }

        return std::static_pointer_cast<Value>(std::make_shared<Integer>(value / otherCast->value));
    }

    std::shared_ptr<Value> operator%(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Integer::unknown();
        }

        auto const* otherCast = static_cast<Integer const*>(&other);

        if (otherCast->value == 0) {
            // TODO: div by 0
        }

        return std::static_pointer_cast<Value>(std::make_shared<Integer>(value % otherCast->value));
    }

    std::shared_ptr<Value> operator!() override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator-() override {
        if (isUnknown()) {
            return unknown();
        }

        return std::static_pointer_cast<Value>(std::make_shared<Integer>(-value));
    }

    [[nodiscard]] Type type() const override {
        return INT;
    }

    const int value;
    const bool _unknown;
};

#endif //LATTE_INT_H
