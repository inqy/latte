#ifndef LATTE_VALUE_H
#define LATTE_VALUE_H

#include <memory>
#include <string>
#include <utility>
#include "../UnsupportedOperationException.h"

enum Type {
    INT,
    STR,
    BOOLEAN,
    OBJECT,
    VOID,
};

class Value {
public:
    [[nodiscard]] virtual Type type() const = 0;
    [[nodiscard]] virtual bool isUnknown() const = 0;
    virtual std::shared_ptr<Value> operator==(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator!=(const Value& other) {
        return !(*(*this == other));
    }
    virtual std::shared_ptr<Value> operator>(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator<(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator>=(const Value& other) {
        return *(*this > other) || *(*this == other);
    }
    virtual std::shared_ptr<Value> operator<=(const Value& other) {
        return *(*this < other) || *(*this == other);
    }

    virtual std::shared_ptr<Value> operator+(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator-(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator||(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator&&(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator*(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator/(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator%(const Value& other) = 0;
    virtual std::shared_ptr<Value> operator!() = 0;
    virtual std::shared_ptr<Value> operator-() = 0;
};

class InvalidTypeException : public std::runtime_error {
public:
    explicit InvalidTypeException() : runtime_error("&other had an invalid type") {}
};



#endif //LATTE_VALUE_H
