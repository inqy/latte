#ifndef LATTE_STR_H
#define LATTE_STR_H

#include <bits/shared_ptr.h>
#include "Value.h"
#include "Boolean.h"

class Str : public Value {
public:
    explicit Str(std::string value) : value(std::move(value)), _unknown(false) {}
    explicit Str() : value(""), _unknown(true) {}

    [[nodiscard]] bool isUnknown() const override {
        return _unknown;
    }

    static std::shared_ptr<Value> default_() {
        std::shared_ptr<Str> ptr = std::make_shared<Str>("\"\"");
        return ptr;
    }

    static std::shared_ptr<Value> unknown() {
        return std::static_pointer_cast<Value>(std::make_shared<Str>());
    }

    static std::shared_ptr<Str> toStr(std::shared_ptr<Value> ptr) {
        return std::static_pointer_cast<Str>(ptr);
    }

    std::shared_ptr<Value> operator==(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Str::unknown();
        }

        auto const* otherCast = static_cast<Str const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value == value));
    }

    std::shared_ptr<Value> operator!=(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Str::unknown();
        }

        auto const* otherCast = static_cast<Str const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value != value));
    }

    std::shared_ptr<Value> operator>(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator>=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator+(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Str::unknown();
        }

        auto const* otherCast = static_cast<Str const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Str>(value + otherCast->value));
    }

    std::shared_ptr<Value> operator-(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator||(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator&&(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator*(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator/(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator%(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator!() override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator-() override {
        throw InvalidTypeException{};
    }

    [[nodiscard]] Type type() const override {
        return STR;
    }

    const std::string value;
    const bool _unknown;
};

#endif //LATTE_STR_H
