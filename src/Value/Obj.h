#ifndef LATTE_OBJ_H
#define LATTE_OBJ_H

#include <bits/shared_ptr.h>
#include "Value.h"

class AnalyzerValueObj : public Value {
public:
    explicit AnalyzerValueObj(std::string type) : userType(std::move(type)) {}

    std::shared_ptr<Value> operator==(const Value& other) override {
        throw UnsupportedOperationException{"ValueObj=="};
    }

    std::shared_ptr<Value> operator!=(const Value& other) override {
        throw UnsupportedOperationException{"ValueObj!="};
    }

    std::shared_ptr<Value> operator>(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator>=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator+(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator-(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator||(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator&&(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator*(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator/(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator%(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator!() override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator-() override {
        throw InvalidTypeException{};
    }

    [[nodiscard]] Type type() const override {
        return OBJECT;
    }

    const std::string userType;
};

#endif //LATTE_OBJ_H
