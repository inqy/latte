#ifndef LATTE_BOOLEAN_H
#define LATTE_BOOLEAN_H

#include <bits/shared_ptr.h>
#include "Value.h"

class Boolean : public Value {
public:
    explicit Boolean(const bool value) : value(value), _unknown(false) {}
    explicit Boolean() : value(false), _unknown(true) {}

    [[nodiscard]] bool isUnknown() const override {
        return _unknown;
    }

    static std::shared_ptr<Value> default_() {
        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(false));
    }

    static std::shared_ptr<Value> unknown() {
        return std::static_pointer_cast<Value>(std::make_shared<Boolean>());
    }

    std::shared_ptr<Value> operator==(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Boolean const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value == value));
    }

    std::shared_ptr<Value> operator!=(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Boolean const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value != value));
    }

    std::shared_ptr<Value> operator>(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator>=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator+(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator-(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator||(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Boolean const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value || value));
    }

    std::shared_ptr<Value> operator&&(const Value& other) override {
        if (other.type() != type()) {
            throw InvalidTypeException{};
        }

        if (isUnknown() || other.isUnknown()) {
            return Boolean::unknown();
        }

        auto const* otherCast = static_cast<Boolean const*>(&other);

        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(otherCast->value && value));
    }

    std::shared_ptr<Value> operator*(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator/(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator%(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator!() override {
        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(!value));
    }

    std::shared_ptr<Value> operator-() override {
        throw InvalidTypeException{};
    }

    [[nodiscard]] Type type() const override {
        return BOOLEAN;
    }

    const bool value;
    const bool _unknown;
};

#include "Value.h"

#endif //LATTE_BOOLEAN_H
