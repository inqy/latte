#ifndef LATTE_NULL_H
#define LATTE_NULL_H

#include <bits/shared_ptr.h>
#include "Value.h"
#include "Boolean.h"

class Null : public Value {
public:

    static std::shared_ptr<Value> null() {
        return std::static_pointer_cast<Value>(std::make_shared<Null>());
    }

    [[nodiscard]] bool isUnknown() const override {
        return false;
    }

    [[nodiscard]] Type type() const override {
        return VOID;
    }

    std::shared_ptr<Value> operator==(const Value& other) override {
        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(other.type() == type()));
    }

    std::shared_ptr<Value> operator!=(const Value& other) override {
        return std::static_pointer_cast<Value>(std::make_shared<Boolean>(other.type() != type()));
    }

    std::shared_ptr<Value> operator>(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator>=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator<=(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator+(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator-(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator||(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator&&(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator*(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator/(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator%(const Value& other) override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator!() override {
        throw InvalidTypeException{};
    }

    std::shared_ptr<Value> operator-() override {
        throw InvalidTypeException{};
    }
};

#endif //LATTE_NULL_H
