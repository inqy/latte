#ifndef LATTE_UNSUPPORTEDOPERATIONEXCEPTION_H
#define LATTE_UNSUPPORTEDOPERATIONEXCEPTION_H

#include <stdexcept>

class UnsupportedOperationException : public std::runtime_error {
public:
    explicit UnsupportedOperationException(std::string arg) : runtime_error(std::move(arg)) {}
};

#endif //LATTE_UNSUPPORTEDOPERATIONEXCEPTION_H
