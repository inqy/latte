#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* readString() {
    char* line;
    size_t n = 0;
    int read = getline(&line, &n, stdin);
    line[read - 1] = '\0';
    return line;
}

int readInt() {
    int n;
    scanf("%d", &n);
    free(readString());
    return n;
}

void printInt(int a) {
    printf("%d\n", a);
}

void printString(char* s) {
    printf("%s\n", s);
}

char* concat(char* a, char* b) {
    int lenA = strlen(a);
    int lenB = strlen(b);
    char* out = malloc(lenA + lenB + 1);
    strcpy(out, a);
    strcpy(out + lenA, b);
    return out;
}
