    .intel_syntax noprefix
    .globl main
main:
    push rbp
    mov rbp, rsp
    sub rsp, 24
    mov rax, 0
    mov [rbp - 8], rax
    mov rdi, [rbp - 8]
    call printInt
    mov [rbp - 16], rax
    mov rax, 0
    mov rsp, rbp
    pop rbp
    ret
    mov rsp, rbp
    pop rbp
    ret
