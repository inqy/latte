#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../src/Compiler.h"

TEST_CASE( "Good stuff is being done", "[compiler]" ) {
    Compiler c;
    REQUIRE( c.compile("../test/lattests/good/core001.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core002.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core003.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core004.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core005.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core006.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core007.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core008.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core009.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core010.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core011.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core012.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core013.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core014.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core015.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core016.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core017.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core018.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core019.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core020.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core021.lat") == 0 );
    REQUIRE( c.compile("../test/lattests/good/core022.lat") == 0 );
}

TEST_CASE( "Bad stuff is not being done", "[compiler]" ) {
    Compiler c;
    REQUIRE( c.compile("../test/lattests/bad/bad001.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad002.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad003.lat") == 1 );

//    REQUIRE( c.compile("../test/lattests/bad/bad004.lat") == 1 ); segfaults
    REQUIRE( c.compile("../test/lattests/bad/bad005.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad006.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad007.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad008.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad009.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad010.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad011.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad012.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad013.lat") == 1 );
//    REQUIRE( c.compile("../test/lattests/bad/bad014.lat") == 1 ); doesnt exist
    REQUIRE( c.compile("../test/lattests/bad/bad015.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad016.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad017.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad018.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad019.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad020.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad021.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad022.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad023.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad024.lat") == 1 );
//    REQUIRE( c.compile("../test/lattests/bad/bad025.lat") == 1 ); different approach
    REQUIRE( c.compile("../test/lattests/bad/bad026.lat") == 1 );
    REQUIRE( c.compile("../test/lattests/bad/bad027.lat") == 1 );
}