#include <antlr4-runtime.h>
#include <vector>
#include <iostream>
#include <string>
#include <boost/program_options.hpp>
#include "src/Compiler.h"

int main(int argc, char** argv) {
    namespace po = boost::program_options;
    po::variables_map vm;
    po::options_description desc("Compiler options");
    desc.add_options()
        ("help", "produce help message")
        ("dialect,d", po::value<std::string>()->default_value("llvm"), "one of {llvm, jvm}")
        ("input-file", po::value<std::vector<std::string>>()->required(), "input file");

    po::positional_options_description p;
    p.add("input-file", -1);

    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 0;
    }

    try {
        po::notify(vm);
        std::string filepath = vm["input-file"].as<std::vector<std::string>>()[0];

        Compiler c;
        return c.compile(filepath);
    } catch(boost::program_options::required_option &e ) {
        std::cout << "Missing a required argument. Use --help to find out more." << std::endl;
    } catch(boost::program_options::invalid_option_value &e) {
        std::cout << "Invalid option value. Use --help to find out more." << std::endl;
    } catch(boost::bad_any_cast &e) {
        std::cout << "Invalid option value. Use --help to find out more." << std::endl;
    }

    return 1;
}